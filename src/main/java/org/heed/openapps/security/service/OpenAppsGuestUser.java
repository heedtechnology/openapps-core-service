package org.heed.openapps.security.service;

import org.heed.openapps.User;

public class OpenAppsGuestUser extends User {

	
	public OpenAppsGuestUser() {
		setId(0L);
	}
	
	@Override
	public boolean hasRole(String role) {
		return false;
	}

	@Override
	public boolean isAdministrator() {
		return false;
	}

	@Override
	public boolean isGuest() {
		return true;
	}
	
}
