package org.heed.openapps.security.service;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.heed.openapps.Group;
import org.heed.openapps.Role;
import org.heed.openapps.SystemModel;
import org.heed.openapps.User;
import org.heed.openapps.cache.CacheService;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityImpl;
import org.heed.openapps.search.SearchRequest;
import org.heed.openapps.search.SearchResponse;
import org.heed.openapps.search.SearchResult;
import org.heed.openapps.search.SearchService;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.net.http.HttpRequest;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.util.RandomPassword;


public class OpenAppsSecurityService implements SecurityService {
	private final static Logger log = Logger.getLogger(OpenAppsSecurityService.class.getName());
	private EntityService entityService;
	private SearchService searchService;
	private CacheService cacheService;
	
	
	@Override
	public int authenticate(String username, String password) {		
		try {
		    User user = getUserByUsername(username);
		    if(user == null) return -1;
		    if(user.getPassword().equals(password))
		    	return 1;
		    else return -2;
		} catch (Exception e) {
			return -1;
		}
	}
	@Override
	public User getCurrentUser(HttpRequest request) {
		String xid = (String)request.getHeaders().get("xid");
		User user = xid != null ? getUserByXid(Long.valueOf(xid)) : null;
		if(user != null) return user;
		return new OpenAppsGuestUser();
	}
	@Override
	public User getUserByXid(long xid) {
		try {
			Entity userEntity = entityService.getEntity(SystemModel.USER, SystemModel.XID, xid);
			User user = getUser(userEntity);
			return user;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public User getUserByUsername(String username) {
		User user = null;
		try {
			if(username.equals("administrator")) {
				user = new User();
				user.setUsername("administrator");
			} else {
				Entity userEntity = entityService.getEntity(SystemModel.USER, SystemModel.USERNAME, username);
				user = getUser(userEntity);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return user;
	}
	@Override
	public User getUserByEmail(String email) {
		try {
			Entity userEntity = entityService.getEntity(SystemModel.USER, SystemModel.EMAIL, email);
			User user = getUser(userEntity);
			return user;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	public List<User> getUsers(String query) {
		List<User> users = new ArrayList<User>();
		System.out.println("getUsers() not implemented");
		return users;
	}
	@Override
	public List<Group> getGroups(String query) {
		List<Group> users = new ArrayList<Group>();
		System.out.println("getGroups() not implemented");
		return users;
	}
	@Override
	public String generatePassword() {
		RandomPassword generator = new RandomPassword();
		return generator.getPass();
	}
	@Override
	public void removeUser(long id) {
		
	}
	@Override
	public void updateUser(User user) {
		try {
			Entity userEntity = entityService.getEntity(user.getId());
			if(user.getPassword() != null && user.getPassword().length() > 0) 
				userEntity.addProperty(SystemModel.USER_PASSWORD, user.getPassword());
			if(user.getFirstName() != null && user.getFirstName().length() > 0) 
				userEntity.addProperty(SystemModel.FIRSTNAME, user.getFirstName());
			if(user.getLastName() != null && user.getLastName().length() > 0) 
				userEntity.addProperty(SystemModel.LASTNAME, user.getLastName());
			if(user.getEmail() != null && user.getEmail().length() > 0) 
				userEntity.addProperty(SystemModel.EMAIL, user.getEmail());
			entityService.updateEntity(userEntity);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	protected User getUser(Entity userEntity) {
		User user = new User();
		user.setId(userEntity.getId());
		user.setFirstName(userEntity.getPropertyValue(SystemModel.FIRSTNAME));
		user.setLastName(userEntity.getPropertyValue(SystemModel.LASTNAME));
		user.setEmail(userEntity.getPropertyValue(SystemModel.EMAIL));
		user.setUsername(userEntity.getPropertyValue(SystemModel.USERNAME));
		return user;
	}
	
	public User addUser(User user) {
		try {
			SearchRequest query = new SearchRequest(SystemModel.USER);
			query.addParameter("xid", String.valueOf(user.getXid()));
			SearchResponse userEntities = searchService.search(query);
			Entity userEntity = null;
			if(userEntities != null && userEntities.getResults().size() > 0) {
				if(userEntities.getResults().size() > 1) {
					for(SearchResult u : userEntities.getResults()) {
						if(userEntity == null) userEntity = u.getEntity();
						else entityService.removeEntity(null, u.getId());
					}
				} else userEntity = userEntities.getResults().get(0).getEntity();
				//cacheService.put("openapps.security.users", String.valueOf(user.getXid()), userEntity.getId());
			} else {
				userEntity = new EntityImpl(SystemModel.USER);
				userEntity.setXid(user.getXid());
				if(user.getPassword() != null && user.getPassword().length() > 0) 
					userEntity.addProperty(SystemModel.USER_PASSWORD, user.getPassword());
				if(user.getFirstName() != null && user.getFirstName().length() > 0) 
					userEntity.addProperty(SystemModel.FIRSTNAME, user.getFirstName());
				if(user.getLastName() != null && user.getLastName().length() > 0) 
					userEntity.addProperty(SystemModel.LASTNAME, user.getLastName());
				if(user.getEmail() != null && user.getEmail().length() > 0) 
					userEntity.addProperty(SystemModel.EMAIL, user.getEmail());
				if(user.getUsername() != null && user.getUsername().length() > 0) {
					userEntity.addProperty(SystemModel.USERNAME, user.getUsername());
					userEntity.addProperty(SystemModel.NAME, user.getUsername());
				}
				Long id = entityService.addEntity(userEntity);
				user.setId(id);
			}
			List<Role> roles = user.getRoles();
			for(Role role : roles) {
				Role r = getRole(role);
				if(r != null) role.setId(r.getId());
			}
			List<Group> groups = user.getGroups();
			for(Group group : groups) {
				Group g = getGroup(group);
				if(g != null) group.setId(g.getId());
			}
		} catch(Exception e) {
			log.log(Level.SEVERE, "", e);
		}
		return user;
	}
	protected Role getRole(Role role) throws Exception {
		Long nodeId = (Long)cacheService.get("openapps.security.roles", String.valueOf(role.getXid()));
		if(nodeId == null) {
			SearchRequest query = new SearchRequest(SystemModel.ROLE);
			query.addParameter("xid", String.valueOf(role.getXid()));
			SearchResponse roleEntities = searchService.search(query);
			Entity roleEntity = null;
			if(roleEntities != null && roleEntities.getResults().size() > 0) {
				if(roleEntities.getResults().size() > 1) {
					for(SearchResult u : roleEntities.getResults()) {
						if(roleEntity == null) roleEntity = u.getEntity();
						else entityService.removeEntity(null, u.getId());
					}
				} else roleEntity = roleEntities.getResults().get(0).getEntity();
				nodeId = roleEntity.getId();
				cacheService.put("openapps.security.roles", String.valueOf(role.getXid()), roleEntity.getId());
			} else {
				roleEntity = new EntityImpl(SystemModel.ROLE);
				if(roleEntity != null) {
					roleEntity.setName(role.getName());
					roleEntity.setXid(role.getXid());
					entityService.addEntity(roleEntity);
					nodeId = roleEntity.getId();
					cacheService.put("openapps.security.roles", String.valueOf(role.getXid()), roleEntity.getId());
				}
			}
		}
		role.setXid(role.getXid());
		role.setName(role.getName());
		role.setId(nodeId);
		return role;
	}
	protected Group getGroup(Group group) throws Exception {
		Long nodeId = (Long)cacheService.get("openapps.security.groups", String.valueOf(group.getXid()));
		if(nodeId == null) {
			SearchRequest query = new SearchRequest(SystemModel.GROUP);
			query.addParameter("xid", String.valueOf(group.getXid()));
			SearchResponse userEntities = searchService.search(query);
			Entity groupEntity = null;
			if(userEntities != null && userEntities.getResults().size() > 0) {
				if(userEntities.getResults().size() > 1) {
					for(SearchResult u : userEntities.getResults()) {
						if(groupEntity == null) groupEntity = u.getEntity();
						else entityService.removeEntity(null, u.getId());
					}
				} else groupEntity = userEntities.getResults().get(0).getEntity();
				nodeId = groupEntity.getId();
				cacheService.put("openapps.security.groups", String.valueOf(group.getXid()), groupEntity.getId());
			} else {
				groupEntity = new EntityImpl(SystemModel.GROUP);
				if(groupEntity != null) {
					groupEntity.setName(group.getName());
					groupEntity.setXid(group.getXid());
					entityService.addEntity(groupEntity);
					nodeId = groupEntity.getId();
					cacheService.put("openapps.security.groups", String.valueOf(group.getXid()), groupEntity.getId());
				}
			}
		}
		group.setXid(group.getXid());
		group.setName(group.getName());
		group.setId(nodeId);
		return group;
	}
	
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	public void setCacheService(CacheService cacheService) {
		this.cacheService = cacheService;
	}
			
}
