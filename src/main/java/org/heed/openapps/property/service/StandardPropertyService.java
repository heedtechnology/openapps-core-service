package org.heed.openapps.property.service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.heed.openapps.property.Property;
import org.heed.openapps.property.PropertyService;


public class StandardPropertyService implements PropertyService {
	private Map<String, String> data = new HashMap<String, String>();
	
		
	@Override
	public void refresh() throws Exception {
		data.clear();
	}
	@Override
	public boolean hasProperty(String name) {
		return data.containsKey(name);
	}
	@Override
	public List<Property> getProperties(String group) {
		List<Property> properties = new ArrayList<Property>();
		for(String key : data.keySet()) {
			if(key.startsWith(group)) 
				properties.add(new Property(group, key, data.get(key)));
		}	
		return properties;
	}

	@Override
	public List<String> getGroupNames() {
		List<String> groups = new ArrayList<String>();
		for(String key : data.keySet()) {
			int idx1 = key.indexOf(".");
			String group = key.substring(0, idx1);
			if(!groups.contains(group))
				groups.add(group);
		}
		return groups;
	}

	@Override
	public Property getProperty(String name) {
		String value =  data.get(name);
		return new Property(name, value);
	}

	@Override
	public String getPropertyValue(String name) {
		return data.get(name);
	}


	@Override
	public int getPropertyValueAsInt(String name) {
		String value = getPropertyValue(name);
		try {
			return Integer.valueOf(value);
		} catch(NumberFormatException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public boolean getPropertyValueAsBoolean(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}	
}
