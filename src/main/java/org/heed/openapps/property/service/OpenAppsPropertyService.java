package org.heed.openapps.property.service;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.heed.openapps.property.Property;
import org.heed.openapps.property.PropertyService;


public class OpenAppsPropertyService implements PropertyService {
	private String configLocation;
	private Properties properties;
	
	
	public OpenAppsPropertyService() {}
	public OpenAppsPropertyService(Properties properties) {
		this.properties = properties;
	}
	public void initialize() {
		loadProperties();
	}
	
	@Override
	public List<String> getGroupNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Property> getProperties(String group) {
		List<Property> properties = new ArrayList<Property>();
		/*
		String[] keys = PropsUtil.getArray(group);
		for(String key : keys) {
			properties.add(getProperty(key));
		}
		*/
		return properties;
	}

	@Override
	public Property getProperty(String name) {
		if(name == null || name.equals("")) return null;
		String value = (String)properties.get(name);
		return new Property(name, value);
	}

	@Override
	public String getPropertyValue(String name) {
		if(name == null || name.equals("")) return null;
		return (String)properties.get(name);
	}

	@Override
	public int getPropertyValueAsInt(String name) {
		if(name == null || name.equals("")) return 0;
		String value = (String)properties.get(name);
		if(value == null || name.equals("")) return 0;
		return Integer.valueOf(value);
	}
	
	@Override
	public boolean getPropertyValueAsBoolean(String name) {
		if(name == null || name.equals("")) return false;
		String value = (String)properties.get(name);
		if(value == null || value.equals("false")) return false;
		return true;
	}
	
	@Override
	public boolean hasProperty(String name) {
		if(name == null || name.equals("")) return false;
		if(properties.get(name) != null) return true;
		return false;
	}

	@Override
	public void refresh() throws Exception {
		loadProperties();
	}
	
	protected void loadProperties() {
		try {
			if(properties == null) properties = new Properties();
			File propertyFile = new File(configLocation);
			if(propertyFile.exists()) {
				FileInputStream stream = new FileInputStream(propertyFile);
				properties.load(stream);
				System.out.println(properties.size()+" properties loaded");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setPropertyFile(String configLocation) {
		this.configLocation = configLocation;
	}	
}
