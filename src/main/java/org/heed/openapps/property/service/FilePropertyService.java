package org.heed.openapps.property.service;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.heed.openapps.property.PropertyService;
import org.heed.openapps.property.Property;


public class FilePropertyService implements PropertyService {
	private String fileName;
	private Map<String, String> data = new HashMap<String, String>();
	
	
	public FilePropertyService(String fileName) {
		this.fileName = fileName;
	}
	
	public void initialize() {
		BufferedReader in = null;
		try {
			File file = new File(fileName);
			in = new BufferedReader(new FileReader(file));
			Properties properties = new Properties();
			properties.load(in);
			
			for(Object key : properties.keySet()) {
				data.put((String)key, (String)properties.get(key));
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	
	@Override
	public void refresh() throws Exception {
		data.clear();
		initialize();
	}
	@Override
	public boolean hasProperty(String name) {
		return data.containsKey(name);
	}
	@Override
	public List<Property> getProperties(String group) {
		List<Property> properties = new ArrayList<Property>();
		for(String key : data.keySet()) {
			if(key.startsWith(group)) 
				properties.add(new Property(group, key, data.get(key)));
		}	
		return properties;
	}

	@Override
	public List<String> getGroupNames() {
		List<String> groups = new ArrayList<String>();
		for(String key : data.keySet()) {
			int idx1 = key.indexOf(".");
			String group = key.substring(0, idx1);
			if(!groups.contains(group))
				groups.add(group);
		}
		return groups;
	}

	@Override
	public Property getProperty(String name) {
		String value =  data.get(name);
		return new Property(name, value);
	}

	@Override
	public String getPropertyValue(String name) {
		return data.get(name);
	}


	@Override
	public int getPropertyValueAsInt(String name) {
		String value = getPropertyValue(name);
		try {
			return Integer.valueOf(value);
		} catch(NumberFormatException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public boolean getPropertyValueAsBoolean(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}	
}
