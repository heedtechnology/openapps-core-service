package org.heed.openapps.content.service;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.IOUtils;
import org.heed.openapps.QName;
import org.heed.openapps.cache.CacheService;
import org.heed.openapps.content.Content;
import org.heed.openapps.content.ContentModel;
import org.heed.openapps.content.EntityContentService;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.AssociationImpl;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.search.SearchRequest;
import org.heed.openapps.search.SearchResponse;
import org.heed.openapps.search.SearchService;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.net.http.HttpRequest;
import org.heed.openapps.net.http.HttpResponse;
import org.heed.openapps.util.IOUtility;


public class NodeContentService extends BaseContentService implements EntityContentService {
	private EntityService entityService;
	private CacheService cacheService;
	private SearchService searchService;
	private List<String> blacklist = new ArrayList<String>();
	
	
	public void initialize() {
		blacklist.add("http://streamerapi.finance.yahoo.com/streamer/1.0");		
	}
	@Override
	public boolean hasContent(String url) {
		url = getUrl(url);
		if(cacheService.has("content", url)) return true;
		SearchRequest query = new SearchRequest(ContentModel.CONTENT);
		query.getProperties().add(new Property(new QName("openapps_org_system_1_0", "name_e"), Property.TYPE_SMALLTEXT, url));
		SearchResponse entities = searchService.search(query);
		//log.info("entity search: " +entities.getResultSize()+" results parsed to " + query.getNativeQuery());
		if(entities.getResults().size() > 0) {
			Entity entity = entities.getResults().get(0).getEntity();
			cacheService.put("content", url, entity.getId());
			return true;
			/*
			File root = getRootDirectory(entity.getId());
			File file = new File(root, entity.getId() + ".bin");
			if(file.exists())
				return true;
				*/
		}
		return false;
	}
	@Override
	public Content getContent(long id) {
		try {
			Content content = new Content(entityService.getEntity(id));
			if(content != null) {
				content.setContent(readContent(content.getId()));
			}
			return content;
		} catch(Exception e) {
			e.printStackTrace();			
		}
		return null;
	}
	@Override
	public Content getContent(String url) {
		url = getUrl(url);
		if(cacheService.has("content", url)) {
			Long id = (Long)cacheService.get("content", url);
			return getContent(id);
		}
		SearchRequest query = new SearchRequest(ContentModel.CONTENT);
		query.getProperties().add(new Property(new QName("openapps_org_system_1_0", "name_e"), Property.TYPE_SMALLTEXT, url));
		SearchResponse entities = searchService.search(query);
		//log.info("entity search: " +entities.getResultSize()+" results parsed to " + query.getNativeQuery());
		Content content = entities.getResults().size() > 0 ? new Content(entities.getResults().get(0).getEntity()) : null;
		if(content == null) {
			content = new Content();
			content.setName(url);
			try {
				entityService.addEntity(content);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(content != null) {
			content.setContent(readContent(content.getId()));
		}
		cacheService.put("content", url, content.getId());
		return content;
	}
	@Override
	public List<Content> getContentByDocument(long id) {
		List<Content> contents = new ArrayList<Content>();
		try {
			Entity document = entityService.getEntity(id);
			if(document != null) {
				List<Association> contentAssocs = document.getSourceAssociations(ContentModel.CONTENTS);
				for(Association contentAssoc : contentAssocs) {
					Content content = new Content(entityService.getEntity(contentAssoc.getTarget()));
					contents.add(content);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return contents;
	}
	public void associateDocumentToContent(long documentId, long contentId) {
		try {
			Entity content = entityService.getEntity(contentId);
			List<Association> assocs = content.getTargetAssociations(ContentModel.CONTENTS);
			boolean exists = false;
			for(Association a : assocs) {
				if(a.getSource() == documentId && a.getTarget() == contentId)
					exists = true;
			}
			if(!exists) 
				entityService.addAssociation(new AssociationImpl(ContentModel.CONTENTS, documentId, contentId));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public byte[] readContent(String url) {
		url = getUrl(url);
		Content content = getContent(url);
		if(content != null) {
			return readContent(content.getId());
		}
		return new byte[0];
	}
	public byte[] readContent(long id) {
		File root = getRootDirectory(id);
		File file = new File(root, id + ".bin");
		if(file.exists()) {
			try {
				FileInputStream input = new FileInputStream(file);
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				IOUtility.pipe(input, output);
				return output.toByteArray();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return new byte[0];
	}
	@Override
	public long writeContent(String url, byte[] data) {
		if(url != null && data != null && data.length > 0) {
			Content content = getContent(url);
			File root = getRootDirectory(content.getId());
			File file = new File(root, content.getId() + ".bin");
			try {
				if(!file.exists()) file.createNewFile();
				FileOutputStream output = new FileOutputStream(file);
				IOUtility.pipe(new ByteArrayInputStream(data), output);
				content.setContent(data);
				return content.getId();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}		
		return 0;
	}
	@Override
	public long writeContent(byte[] request, byte[] response) {
		try {
			HttpRequest httpRequest = new HttpRequest(request);
			if(httpRequest != null) {
				String url = getUrl(httpRequest);
				if(acceptWriteRequest(httpRequest)) {
					System.out.println("WRITE - "+url);
					HttpResponse httpResponse = new HttpResponse();
					httpResponse.setContentBytes(response);
					if(acceptWriteResponse(httpResponse)) {				
						Content content = getContent(url);
						long documentId = getRequestDocument(httpRequest);
						if(documentId > 0) associateDocumentToContent(documentId, content.getId());
						byte[] data = httpResponse.getContentBytes();
						if(url != null && data != null && data.length > 0) {
							File root = getRootDirectory(content.getId());
							File file = new File(root, content.getId() + ".bin");
							try {
								if(!file.exists()) file.createNewFile();
								FileOutputStream output = new FileOutputStream(file);
								if(httpResponse.getEncoding() != null && httpResponse.getEncoding().equals("gzip")) {
									InputStream in = new GZIPInputStream(new ByteArrayInputStream(httpResponse.getContentBytes()));
									IOUtils.copy(in, output);
								} else {
									IOUtils.write(httpResponse.getContent(), output);
									content.setContent(httpResponse.getContentBytes());
								}
								return content.getId();
							} catch(Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		} catch(Exception e) {
			//log.error("error writing content: "+new String(request));			
		}
		return 0;
	}
		
	private File getRootDirectory(long id) {
		String idStr = String.valueOf(id);
		File root = new File("data/content");
		if(!root.exists()) root.mkdir();
		for(int i=0; i < idStr.length(); i++) {
			root = new File(root, Character.toString(idStr.charAt(i)));
			if(!root.exists()) root.mkdir();
		}
		return root;
	}
	private boolean acceptWriteRequest(HttpRequest httpRequest) {
		String url = getUrl(httpRequest);
		for(String str : blacklist) {
			if(url.contains(str))
				return false;
		}
		if(url.endsWith(".gif") || 
				url.endsWith(".jpg") || 
				url.endsWith(".png") || 
				url.endsWith(".css") ||
				url.endsWith(".html") ||
				url.endsWith(".htm")) {
			url = getUrl(url);
			SearchRequest query = new SearchRequest(ContentModel.CONTENT);
			query.getProperties().add(new Property(new QName("openapps_org_system_1_0", "name_e"), Property.TYPE_SMALLTEXT, url));
			SearchResponse entities = searchService.search(query);
			return entities.getResults().size() == 0;
		}
		return false;
	}
	private boolean acceptWriteResponse(HttpResponse httpResponse) {
		if(httpResponse == null) return false;
		//if(httpResponse.contentType().equals(".js") || httpRequest.path().endsWith(".css")) {
		
		return true;
	}
	private String getUrl(HttpRequest request) {
		StringWriter writer = new StringWriter();
		try {
			URL url = new URL(request.getUrl());
			writer.write(url.getProtocol()+"://");
			writer.write(url.getHost());
			writer.write(url.getPath());
			//if(request.queryString() != null && request.queryString().length() > 0)
				//writer.write("?"+request.queryString());
		} catch(Exception e) {
			e.printStackTrace();
		}
		return writer.toString();
	}
	private long getRequestDocument(HttpRequest request) {
		//String userAgent = null;//request.header("user-agent");
		//String windowsString = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36";
		/*
		if(userAgent != null && userAgent.startsWith(windowsString)) {
			String userAgentId = userAgent.substring(windowsString.length());
			if(userAgentId != null && userAgentId.length() > 0) {
				long documentId = Long.valueOf(userAgentId.trim());
				return documentId;
			}
		}
		*/
		return 0;
	}
	public String getUrl(String in) {
		int pound = in.indexOf("?");
		if(pound > -1) in = in.substring(0, pound);
		return in;//in.replace("http://", "").replace("https://", "").trim();
	}
	public String getDomain(String url) {
		if(url == null || url.length() < 4) return "";
		try {
			URI uri = new URI(url);
			String domain = uri.getHost();
			if(domain != null) {
				domain = domain.replace("google2.", "");
				if(domain.startsWith("www.")) domain = domain.substring(4);
				return domain;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	/*
	public Map<String,String> readWarc(String id) throws Exception {
		Map<String,String> data = new HashMap<String,String>();
		String root = "data/content";
		for(int i=0; i < id.length(); i++) {
			root = root + "/" + id.charAt(i);
		}
		File file = new File(root, id + ".warc");
		if(file.exists()) {
			//temp directory where we will stage the files in the warc 
			//String tmpdir = System.getProperty("java.io.tmpdir") + "opencrawler";
	    	//File rootDir = new File(tmpdir);
	    	//if(!rootDir.exists()) rootDir.mkdir();
	    	//File directory = new File(rootDir, "content");
	    	///if(!directory.exists()) directory.mkdir();
			//BufferedInputStream is = new BufferedInputStream(new FileInputStream(file));
			ArchiveReader ar = WARCReaderFactory.get(file.getAbsolutePath(), is, true);
			for(ArchiveRecord r : ar) {
				String url = r.getHeader().getUrl();
				if(url != null) {
					String recordId = (String)r.getHeader().getHeaderValue("WARC-Record-ID");
		    		if(recordId != null && recordId.length() > 0) {
		    			recordId = recordId.replace("<urn:uuid:", "").replace(">", "");
		    			cacheService.put("content", url, recordId);
						data.put(url, recordId);
						
						File tempFile = new File(directory, recordId + ".bin");
			    		FileOutputStream out = new FileOutputStream(tempFile, false);
			    		IOUtils.copy(r, out);			    		
		    		}
					//byte[] dataArr = IOUtils.toByteArray(r);
				}
			}			
		}
		return data;
	}
	public long writeWarc(byte[] request, byte[] response) throws Exception {
		HttpRequest httpRequest = new HttpRequest(request);
		HttpResponse httpResponse = new HttpResponse(response);
		if(httpRequest != null && httpResponse != null) {
			String url = getUrl(httpRequest);
			if(acceptWriteRequest(httpRequest)) {
				Content content = getContent(url);
				long documentId = getRequestDocument(httpRequest);
				if(documentId > 0) associateDocumentToContent(documentId, content.getId());
				byte[] data = httpResponse.getContent();
				if(url != null && data != null && data.length > 0) {
					File root = new File("data/content");
					if(!root.exists()) root.mkdir();
					String id = String.valueOf(content.getId());
					for(int i=0; i < id.length(); i++) {
						root = new File(root, Character.toString(id.charAt(i)));
						if(!root.exists()) root.mkdir();
					}
					AtomicInteger SERIAL_NO = new AtomicInteger();
					File [] files = {root};
					UUIDGenerator generator = new UUIDGenerator();
					WARCWriterPoolSettings settings = new WARCWriterPoolSettingsData(this.getClass().getName(), id, -1, false, Arrays.asList(files), null, generator);
					WARCWriter writer =	new WARCWriter(SERIAL_NO, settings);
					try {			
						WARCRecordInfo recordInfo = new WARCRecordInfo();
						recordInfo.setType(WARCRecordType.resource);
						recordInfo.setUrl(url);
						recordInfo.setCreate14DigitDate(ArchiveUtils.get14DigitDate());
						recordInfo.setMimetype(httpResponse.getContentType());
						recordInfo.setRecordId(generator.getRecordID());
						recordInfo.setContentStream(new ByteArrayInputStream(httpResponse.getContent()));
						recordInfo.setContentLength((long)httpResponse.getContent().length);
						ANVLRecord headerRecord = new ANVLRecord();
						Map<String, String[]> headers = new HashMap<String,String[]>();//httpResponse.headers();
						for(String key : headers.keySet()) {
							headerRecord.addLabel(key);
							headerRecord.addLabelValue(key, headers.get(key)[0]);
						}
						recordInfo.setExtraHeaders(headerRecord);
						writer.writeRecord(recordInfo);
					} finally {
						writer.close();
					}
				}
			}
		}
		return 0;
	}
	*/
	public EntityService getEntityService() {
		return entityService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public CacheService getCacheService() {
		return cacheService;
	}
	public void setCacheService(CacheService cacheService) {
		this.cacheService = cacheService;
	}
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	@Override
	public void removeContent(long arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public byte[] load(String arg0, boolean arg1) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public long processImage(String arg0, int arg1, int arg2, boolean arg3) {
		// TODO Auto-generated method stub
		return 0;
	}
				
}
