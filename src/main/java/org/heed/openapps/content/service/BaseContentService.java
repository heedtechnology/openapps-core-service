package org.heed.openapps.content.service;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.heed.openapps.content.ContentService;
import org.heed.openapps.net.http.HttpComponent;
import org.heed.openapps.net.http.HttpProxy;
import org.heed.openapps.net.http.HttpResponse;
import org.heed.openapps.util.IOUtility;


public class BaseContentService implements ContentService {
	private HttpComponent http;
	private List<String> blacklist = new ArrayList<String>();
	private List<String> contentTypes = new ArrayList<String>();
	private String rootDirectory = "";
	
	
	public BaseContentService() {
		initialize();
	}
	
	public void initialize() {
		blacklist.add("http://streamerapi.finance.yahoo.com/streamer/1.0");
		
		contentTypes.add("image/jpeg");
		contentTypes.add("image/gif");
		contentTypes.add("image/png");
		//contentTypes.add("image/svg+xml");
		
		//contentTypes.add("text/css");
		
		//contentTypes.add("text/html");
		//contentTypes.add("application/x-shockwave-flash");
		//contentTypes.add("application/x-mpegURL");
		
		/*
		contentTypes.add("application/javascript");
		contentTypes.add("application/x-javascript");
					
		contentTypes.add("text/plain");
		contentTypes.add("text/xml");
		contentTypes.add("application/json");
		*/
		
	}
	
	public byte[] read(long id) {
		File root = getRootDirectory(id);
		File file = new File(root, id + ".bin");
		if(file.exists()) {
			try {
				FileInputStream input = new FileInputStream(file);
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				IOUtility.pipe(input, output);
				return output.toByteArray();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return new byte[0];
	}
	public void write(long id, byte[] content) {
		if(content == null || content.length == 0) return;
		File root = getRootDirectory(id);
		File file = new File(root, id + ".bin");
		//Content c = getContent(id);
		try {			
			if(!file.exists()) file.createNewFile();			
			FileOutputStream output = new FileOutputStream(file);
			IOUtility.pipe(new ByteArrayInputStream(content), output);
			//c.setContent(content);
			//cacheService.put("content", c.getName(), c.getId());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/*
	public long processImage(String icon, int targetHeight, int targetWidth, boolean shouldProxy) {
		Content c = getContent(icon);
		if(c == null) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] iconContent = load(icon, shouldProxy);
			BufferedImage rawImage = null;
			try {
				if(icon.endsWith(".jpg")) 
					rawImage = JPEGImageIO.read(new ByteArrayInputStream(iconContent));
				else if(icon.endsWith(".gif")) 
					rawImage = GIFDecoder.read(new ByteArrayInputStream(iconContent)).getFrame(0);
				else 
					rawImage = ImageIO.read(new ByteArrayInputStream(iconContent));
				if(rawImage != null) {
					if(rawImage.getHeight() > targetHeight || rawImage.getWidth() > targetWidth) {
						BufferedImage scaledImage = Scalr.resize(rawImage, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.AUTOMATIC, targetWidth, targetHeight, Scalr.OP_ANTIALIAS);
						BufferedImage combined = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB);
						int x = Math.round((targetWidth - scaledImage.getWidth()) / 2);
						int y = Math.round((targetHeight - scaledImage.getHeight()) / 2);
						Graphics2D	graphics = combined.createGraphics();
						graphics.setPaint(Color.BLACK);
						graphics.fillRect(0, 0, targetWidth, targetHeight);
						graphics.drawImage(scaledImage, x, y, null);
						ImageIO.write(combined, "png", baos);
					} else {
						ImageIO.write(rawImage, "png", baos);
					}
					baos.flush();
					byte[] payload = baos.toByteArray();
					baos.close();				
					long contentId = writeContent(icon, payload);
					return contentId;
				}
			} catch(Exception e) {
				if(rawImage != null) {
					try {
						ImageIO.write(rawImage, "png", baos);
						baos.flush();
						byte[] payload = baos.toByteArray();
						baos.close();						
						long contentId = writeContent(icon, payload);
						return contentId;
					} catch(Exception e2) {
						e2.printStackTrace();
					}
				}				
			}
		} 
		return 0;
	}
	*/
	public byte[] load(String url, boolean shouldProxy) {
		HashMap<String,String> headers = new HashMap<String,String>();
		headers.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36");
		boolean proxyAvailable = Boolean.getBoolean("skillet.system.proxy");
		try {
			HttpResponse response = (proxyAvailable && shouldProxy) ? http.get(url, new HttpProxy("127.0.0.1", 9151, "HTTP"), headers, null) : http.get(url, headers, null);
			return response.getContentBytes();			
		} catch(Exception e) {
			System.out.println("connection exception : "+url);
			e.printStackTrace();
		}
		return new byte[0];
	}
	public void remove(long id) {
		File root = getRootDirectory(id);
		File file = new File(root, id + ".bin");
		//Content c = getContent(id);
		try {			
			if(file.exists()) {
				file.delete();
				root.delete();
			}			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	private File getRootDirectory(long id) {
		String idStr = String.valueOf(id);
		File root = new File(rootDirectory);
		if(!root.exists()) root.mkdir();
		for(int i=0; i < idStr.length(); i++) {
			root = new File(root, Character.toString(idStr.charAt(i)));
			if(!root.exists()) root.mkdir();
		}
		return root;
	}
	public String getDomain(String url) {
		try {
			URI uri = new URI(url);
			String domain = uri.getHost();
			return domain.startsWith("www.") ? domain.substring(4) : domain;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	public String getUrl(String in) {
		String out = in.replace("%23", "#").trim();
		//out = in.replace("http://www.", "http://");
		if(out.contains(";jsessionid=")) {
			int start = out.indexOf(";jsessionid=");
			if(out.contains("?")) {
				int end = out.indexOf("?");
				if(end > start) {
					out = out.substring(0, start) + out.substring(end);
				}
			} else {
				out = out.substring(0, start);
			}
		}
		return out;
	}
	public void setRootDirectory(String rootDirectory) {
		this.rootDirectory = rootDirectory;
	}
	public HttpComponent getHttpComponent() {
		return http;
	}
	public void setHttpComponent(HttpComponent http) {
		this.http = http;
	}
	
}
