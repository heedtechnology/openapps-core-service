package org.heed.openapps.search.service;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonReader;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.net.http.HttpResponse;
import org.heed.openapps.net.http.RestUtility;
import org.heed.openapps.scheduling.Job;
import org.heed.openapps.search.SearchRequest;
import org.heed.openapps.search.SearchResponse;
import org.heed.openapps.search.SearchService;



public class RestSearchService implements SearchService {
	private static final long serialVersionUID = 6775657745858562682L;
	private final static Logger log = Logger.getLogger(RestSearchService.class.getName());
	private String serviceUrl;
	
	
	public RestSearchService() {}
	public RestSearchService(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	
	@Override
	public SearchResponse search(SearchRequest query) {
		SearchResponse results = new SearchResponse();
		try {
			String json = query.toJsonObject().toString();
			String url = serviceUrl+"/service/search/entity.json";
			HttpResponse response = RestUtility.httpPost(url, json.getBytes("UTF-8"), getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				//JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        //results.fromJson(jsonObject);
			} else log.info("HTTP "+response.getStatusCode()+" - "+url);
		} catch(Exception e) {
			log.log(Level.SEVERE, "", e);
		}		
		return results;
	}
	/*
	@Override
	public SearchResponse search(SearchRequest request) {
		SearchResponse searchResponse = new SearchResponse();
		try {
			String json = request.toJsonObject().toString();
			HttpResponse response = RestUtility.httpPost(serviceUrl+"/service/search/request.json", json.getBytes("UTF-8"), getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        searchResponse.fromJsonObject(jsonObject);
			}
		} catch(Exception e) {
			log.log(Level.SEVERE, "", e);
		}
		return searchResponse;
	}
	*/	
	protected Map<String,String> getHeaders() {
		Map<String,String> headers = new HashMap<String,String>();
		headers.put("Content-Type", "application/json;charset=UTF-8");
		headers.put("Accept", "application/json;charset=UTF-8");
		
		return headers;
	}
	
	@Override
	public int count(SearchRequest arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void remove(Long arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Job update(QName arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Entity arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(List<Entity> arg0) {
		// TODO Auto-generated method stub
		
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	
	
}
