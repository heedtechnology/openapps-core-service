package org.heed.openapps.proxy.service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.heed.openapps.data.csv.CSVParser;
import org.heed.openapps.net.http.HttpComponent;
import org.heed.openapps.net.http.HttpProxy;
import org.heed.openapps.net.http.HttpResponse;
import org.heed.openapps.net.http.URLConnectionComponent;
import org.heed.openapps.proxy.ProxyService;


public class PublicProxyService implements ProxyService {
	private HttpComponent http = new URLConnectionComponent();
	private List<String> proxies = new ArrayList<String>();
	
	
	@Override
	public byte[] get(String url, Map<String, String> headers) {
		HttpResponse response = null;
		Collections.shuffle(proxies);
		String address = proxies.get(0);
		try {			
			String[] rec = address.split(":");
			response = http.get(url, new HttpProxy(rec[0], Integer.parseInt(rec[1]), "HTTP"), headers, null);
		} catch(Exception e) {
			System.out.println("problem fetching : "+url+" via "+address);
			return new byte[0];
		}
		return response.getContentBytes();		
	}

	@Override
	public void start() {
		CSVParser httpParser = new CSVParser();
		try {
			httpParser.process("http://www.heedtechnology.com/downloads/proxies.csv");
			for(String[] record : httpParser.getData()) {
				String address = record[0].replaceAll("^\"|\"$", "");
				proxies.add(address);
			}			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stop() {
		
	}

}
