package org.heed.openapps.dictionary.service;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelFieldValue;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.util.XMLUtility;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXModelImporter2 {
	private List<Model> systemModels;
	private DataDictionary dictionary;
	
	
	public void process(InputStream stream, List<Model> systemModels) throws Exception {
		this.systemModels = systemModels;
		XMLUtility.SAXParse(false, stream, new ImportHandler());
	}
	public DataDictionary getDictionary() {
		return dictionary;
	}
		
	public class ImportHandler extends DefaultHandler {
		StringBuffer buff = new StringBuffer();
		Map<String,String> namespaces = new HashMap<String,String>();
		private List<Model> models = new ArrayList<Model>();
		Model model;
		ModelRelation relation;
		ModelField field;		
		
		public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException	{
			if(qName.equals("model")) {
				String namespaceAttr =  attrs.getValue("namespace");
				String localnameAttr =  attrs.getValue("localname");
				if(namespaceAttr != null && localnameAttr != null) {
					model = new Model(null, new QName(namespaceAttr, localnameAttr));
					model.setUid(java.util.UUID.randomUUID().toString());
					
					String searchable = attrs.getValue("searchable");
					if(searchable != null && searchable.equals("true")) model.setSearchable(true);
					else model.setSearchable(false);
					
					models.add(model);
				}
			} else if(qName.equals("property")) {
				String namespaceAttr =  attrs.getValue("namespace");
				String localnameAttr =  attrs.getValue("localname");
				if(namespaceAttr != null && localnameAttr != null) {
					field = new ModelField(0, new QName(namespaceAttr, localnameAttr));
					field.setUid(java.util.UUID.randomUUID().toString());
					String type = attrs.getValue("type");
					if(type != null && type.length() > 0) {
						field.setType(type);
					}
					String format = attrs.getValue("format");
					if(format != null && format.length() > 0) {
						field.setFormat(Integer.valueOf(format));
					}
					String mandatory = attrs.getValue("mandatory");
					if(mandatory != null && mandatory.length() > 0) {
						field.setMandatory(Boolean.valueOf(type));
					}
					String unique = attrs.getValue("unique");
					if(unique != null && unique.length() > 0) {
						field.setUnique(Boolean.valueOf(unique));
					}
					String hidden = attrs.getValue("hidden");
					if(hidden != null && hidden.length() > 0) {
						field.setHidden(Boolean.valueOf(hidden));
					}
					String label = attrs.getValue("label");
					if(label != null && label.length() > 0) {
						field.setLabel(label);
					}					
					String searchable = attrs.getValue("searchable");
					if(searchable != null && searchable.equals("true")) 
						field.setSearchable(true);
					else model.setSearchable(false);					
					
					String sortable = attrs.getValue("sortable");
					if(sortable != null && sortable.equals("true")) field.setSortable(true);
					else field.setSortable(false);
					
					models.add(model);
				}
			} else if(qName.equals("association")) {
				String namespaceAttr =  attrs.getValue("namespace");
				String localnameAttr =  attrs.getValue("localname");
				if(namespaceAttr != null && localnameAttr != null) {
					String many = attrs.getValue("many");
					if(many == null) many = "false";
					String cascade = attrs.getValue("cascade");
					if(cascade == null) cascade = "false";
					relation = new ModelRelation(null, model.getQName(), null, ModelRelation.DIRECTION_OUTGOING, new QName(namespaceAttr, localnameAttr));
					relation.setUid(java.util.UUID.randomUUID().toString());
					relation.setMany(Boolean.parseBoolean(many));
					relation.setCascade(Boolean.parseBoolean(cascade));
				}
			} else if(qName.equals("target")) {
				String namespaceAttr =  attrs.getValue("namespace");
				String localnameAttr =  attrs.getValue("localname");
				if(relation != null && namespaceAttr != null && localnameAttr != null) {
					relation.setEndName(new QName(namespaceAttr, localnameAttr));
				}
			} else if(qName.equals("parent")) {
				String namespaceAttr =  attrs.getValue("namespace");
				String localnameAttr =  attrs.getValue("localname");
				if(model != null && namespaceAttr != null && localnameAttr != null) {
					model.setParentName(new QName(namespaceAttr, localnameAttr));
				}
			} else if(qName.equals("dictionary")) {
				dictionary = new DataDictionary();
			}
		}
	
		public void characters(char[] ch, int start, int length) throws SAXException {
			buff.append(ch,start,length);
		}
	
		public void endElement(String namespaceURI, String sName, String qName) throws SAXException	{
			String data = buff.toString().trim();
			if(qName.equals("value")) {
				field.getValues().add(new ModelFieldValue(data, data));
			} else if(qName.equals("type") && data != null && field != null) {
				field.setType(data);
			} else if(qName.equals("property")) {
				if(relation != null) relation.getFields().add(field);
				else if(model != null) model.getFields().add(field);
			} else if(qName.equals("association") && relation != null) {
				model.getSourceRelations().add(relation);
				relation = null;
			} else if(qName.equals("name")) {
				if(model != null) model.setName(data);
				else if(dictionary != null) dictionary.setName(data);
			} else if(qName.equals("description")) {
				if(model != null) model.setDescription(data);
				else if(dictionary != null) dictionary.setDescription(data);
			} else if(qName.equals("dictionary")) {
				dictionary.setLocalModels(models);
			}
			buff = new StringBuffer();
		}
		
		protected Model getModel(String namespace, String localname) {
			for(Model model : models) {
				if(model.getQName().getNamespace().equals(namespace) && model.getQName().getLocalName().equals(localname))
					return model;
			}
			for(Model model : systemModels) {
				if(model.getQName().getNamespace().equals(namespace) && model.getQName().getLocalName().equals(localname))
					return model;
			}
			return null;
		}
	}
}
