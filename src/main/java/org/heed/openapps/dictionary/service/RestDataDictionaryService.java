package org.heed.openapps.dictionary.service;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.heed.openapps.QName;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityImpl;
import org.heed.openapps.net.http.HttpResponse;
import org.heed.openapps.net.http.RestUtility;


public class RestDataDictionaryService implements DataDictionaryService {
	private static final long serialVersionUID = 885673825642079487L;
	private String serviceUrl;
	
	
	public RestDataDictionaryService(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	
	@Override
	public List<DataDictionary> getBaseDictionaries() {
		List<DataDictionary> list = new ArrayList<DataDictionary>();
		try {
			HttpResponse response = RestUtility.httpGet(serviceUrl+"/service/dictionary/base.json", getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				for(int i=0; i < data.size(); i++) {
					JsonObject entityObj = data.getJsonObject(i);
					Entity entity = new EntityImpl(entityObj);
				}
			}
		} catch(Exception e) {
			
		}
		return list;
	}

	@Override
	public DataDictionary getDataDictionary(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataDictionary getSystemDictionary() {
		// TODO Auto-generated method stub
		return null;
	}

	protected Map<String,String> getHeaders() {
		Map<String,String> headers = new HashMap<String,String>();
		
		
		return headers;
	}

	@Override
	public Model getModel(QName association, QName startNode, QName endNode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Model getModel(QName qname) {
		// TODO Auto-generated method stub
		return null;
	}
}
