package org.heed.openapps.dictionary.service;
import java.util.List;

import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.search.SearchService;

import org.heed.openapps.entity.EntityService;


public class DataDictionaryServiceImpl extends DataDictionarySupport {
	private static final long serialVersionUID = -8594531361920252635L;
	protected EntityService entityService;
	protected SearchService searchService;
		
	
	public void initialize() {			
		try	{
			//system dictionary
			systemDictionary.setId(0L);
			systemDictionary.setUid(java.util.UUID.randomUUID().toString());
			List<Model> systemModels = processImports(systemImports);
			systemDictionary.setLocalModels(systemModels);
			System.out.println("system data dictionary loaded, "+systemModels.size()+" models loaded...");
						
			//base dictionaries
			for(String importFile : baseImports) {						
				List<Model> baseModels = processImport(importFile);
				DataDictionary dictionary = new DataDictionary();
				String name = importFile.replace(".xml", "");
				dictionary.setName(name);
				dictionary.setLocalModels(baseModels);
				baseDictionaries.put(name, dictionary);
				System.out.println("base data dictionary " +name+" loaded, "+baseModels.size()+" models loaded...");
			}			
		} catch(Exception e) {
			e.printStackTrace();
		} 
	}
	
}
