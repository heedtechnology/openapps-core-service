package org.heed.openapps.dictionary.service;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;







import org.heed.openapps.QName;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelFieldAspect;
import org.heed.openapps.dictionary.ModelFieldValue;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.util.XMLUtility;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXModelImporter {
	private List<Model> systemModels;
	private List<Model> models = new ArrayList<Model>();
	
	
	public void process(InputStream stream, List<Model> systemModels) throws Exception {
		this.systemModels = systemModels;
		XMLUtility.SAXParse(false, stream, new ImportHandler());
	}
	public List<Model> getModels() {
		return models;
	}
		
	public class ImportHandler extends DefaultHandler {
		StringBuffer buff = new StringBuffer();
		Map<String,String> namespaces = new HashMap<String,String>();
		Model model;
		ModelRelation relation;
		ModelField field;
		ModelFieldAspect aspect;
		ModelField aspectField;
		
		
		public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException	{
			if(qName.equals("namespace") || qName.equals("import")) {
				namespaces.put(attrs.getValue("prefix"), attrs.getValue("uri"));
			} else if(qName.equals("type")) {
				String nameAttr =  attrs.getValue("name");
				if(nameAttr != null) {
					String[] name = nameAttr.split(":");
					String namespace = namespaces.get(name[0]);
					model = new Model(null, new QName(namespace, name[1]));
					models.add(model);
				}
			} else if(qName.equals("title")) {
				
			} else if(qName.equals("property")) {
				String[] name = attrs.getValue("name").split(":");
				String namespace = namespaces.get(name[0]);
				if(aspect != null) aspectField = new ModelField(0, new QName(namespace, name[1]));
				else field = new ModelField(0, new QName(namespace, name[1]));				
			} else if(qName.equals("association")) {
				String[] name = attrs.getValue("name").split(":");
				String namespace = namespaces.get(name[0]);
				String many = attrs.getValue("many");
				if(many == null) many = "false";
				String cascade = attrs.getValue("cascade");
				if(cascade == null) cascade = "false";
				relation = new ModelRelation(null, model.getQName(), null, ModelRelation.DIRECTION_OUTGOING, new QName(namespace, name[1]));
				relation.setMany(Boolean.parseBoolean(many));
				relation.setCascade(Boolean.parseBoolean(cascade));
			} else if(qName.equals("aspect")) {
				String nameAttr = attrs.getValue("name");
				if(nameAttr != null) {
					String[] name = nameAttr.split(":");
					String namespace = namespaces.get(name[0]);
					aspect = new ModelFieldAspect(null, new QName(namespace, name[1]));
				}
			} 
		}
	
		public void characters(char[] ch, int start, int length) throws SAXException {
			buff.append(ch,start,length);
		}
	
		public void endElement(String namespaceURI, String sName, String qName) throws SAXException	{
			String data = buff.toString().trim();
			if(qName.equals("value")) {
				if(aspect != null) aspectField.getValues().add(new ModelFieldValue(data, data));
				else field.getValues().add(new ModelFieldValue(data, data));
			} else if(qName.equals("type") && data != null) {
				if(data.equals("sys:text") && field != null) field.setType(ModelField.TYPE_SMALLTEXT);
				else if(data.equals("sys:longtext") && field != null) field.setType(ModelField.TYPE_LONGTEXT);
				else if(data.equals("sys:date") && field != null) field.setType(ModelField.TYPE_DATE);
			} else if(qName.equals("property")) {
				if(aspect != null) aspect.getFields().add(aspectField);
				else if(relation != null) relation.getFields().add(field);
				else if(model != null) model.getFields().add(field);
			} else if(qName.equals("association") && relation != null) {
				model.getSourceRelations().add(relation);
				relation = null;
			} else if(qName.equals("aspect") && aspect != null) {
				field.getAspects().add(aspect);
				aspect = null;
			} else if(qName.equals("parent")) {
				String[] name = data.toString().split(":");
				String namespace = namespaces.get(name[0]);
				Model parentModel = getModel(namespace, name[1]);
				if(model != null && parentModel != null) {
					model.setParentName(parentModel.getQName());
				}
			} else if(qName.equals("class")) {
				String[] name = data.toString().split(":");
				String namespace = namespaces.get(name[0]);
				relation.setEndName(new QName(namespace, name[1]));
			} else if(qName.equals("title")) {
				if(aspect != null) aspect.setDescription(data);
				else model.setDescription(data);
			} 
			buff = new StringBuffer();
		}
		
		protected Model getModel(String namespace, String localname) {
			for(Model model : models) {
				if(model.getQName().getNamespace().equals(namespace) && model.getQName().getLocalName().equals(localname))
					return model;
			}
			for(Model model : systemModels) {
				if(model.getQName().getNamespace().equals(namespace) && model.getQName().getLocalName().equals(localname))
					return model;
			}
			return null;
		}
	}
}
