package org.heed.openapps.dictionary.service;
import java.util.List;

import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.Model;

import org.heed.openapps.property.PropertyService;


public class XmlDataDictionaryService extends DataDictionarySupport {
	private static final long serialVersionUID = -8594531361920252635L;
	
	
	public void initialize() {			
		try	{
			long index = 1;
			//system dictionary
			getSystemDictionary().setId(0L);
			getSystemDictionary().setUid(java.util.UUID.randomUUID().toString());
			List<Model> systemModels = processImports(systemImports);
			getSystemDictionary().setLocalModels(systemModels);
			System.out.println("system data dictionary loaded, "+systemModels.size()+" models loaded...");
						
			//base dictionaries
			for(String importFile : baseImports) {						
				List<Model> baseModels = processImport(importFile);
				DataDictionary dictionary = new DataDictionary();
				dictionary.setId(index);
				String name = importFile.replace(".xml", "");
				dictionary.setName(name);
				dictionary.setLocalModels(baseModels);
				baseDictionaries.put(name, dictionary);
				System.out.println("base data dictionary " +name+" loaded, "+baseModels.size()+" models loaded...");
				index++;
			}			
		} catch(Exception e) {
			e.printStackTrace();
		} 
	}
	
	
	
	/*
	@Override
	public DataDictionary reloadDataDictionary(long dictionaryid) throws DataDictionaryException {
		try {
			refreshDictionary(dictionaryid);
			return userDictionaries.get(dictionaryid);
		} catch(Exception e) {
			throw new DataDictionaryException("problem reloading dictionary id:"+dictionaryid, e);
		}
	}
	@Override
	public void remove(long id) throws DataDictionaryException {
		try	{
			Entity entity = entityService.getEntity(id);
			if(entity.getQName().equals(DictionaryModel.DICTIONARY)) {
				List<Association> modelsAssoc = entity.getSourceAssociations(DictionaryModel.MODELS);
				for(Association modelAssoc : modelsAssoc) {
					Entity modelEntity = entityService.getEntity(modelAssoc.getTarget());
					removeModel(modelEntity);
				}
				entityService.removeEntity(entity.getId());
				userDictionaries.remove(entity.getId());
			} else if(entity.getQName().equals(DictionaryModel.MODEL)) {
				Association dictionaryAssoc = entity.getTargetAssociation(DictionaryModel.MODELS);
				removeModel(entity);
				refreshDictionary(dictionaryAssoc.getSource());
			} else if(entity.getQName().equals(DictionaryModel.MODEL_FIELD))	{
				Association modelAssoc = entity.getTargetAssociation(DictionaryModel.MODEL_FIELD_VALUES);
				Entity model = entityService.getEntity(modelAssoc.getSource());
				Association dictionaryAssoc = model.getTargetAssociation(DictionaryModel.MODELS);
				entityService.removeEntity(entity.getId());
				refreshDictionary(dictionaryAssoc.getSource());				
			} else if(entity.getQName().equals(DictionaryModel.MODEL_RELATION))	{
				Association modelAssoc = entity.getTargetAssociation(DictionaryModel.MODEL_RELATIONS);
				Entity model = entityService.getEntity(modelAssoc.getSource());
				Association dictionaryAssoc = model.getTargetAssociation(DictionaryModel.MODELS);
				entityService.removeEntity(entity.getId());
				refreshDictionary(dictionaryAssoc.getSource());				
			} else if(entity.getQName().equals(DictionaryModel.MODEL_FIELD_VALUE))	{
				Association fieldAssoc = entity.getTargetAssociation(DictionaryModel.MODEL_FIELD_VALUES);
				Entity field = entityService.getEntity(fieldAssoc.getSource());
				Association modelAssoc = field.getTargetAssociation(DictionaryModel.MODEL_FIELDS);
				Entity model = entityService.getEntity(modelAssoc.getSource());
				Association dictionaryAssoc = model.getTargetAssociation(DictionaryModel.MODELS);
				entityService.removeEntity(entity.getId());
				refreshDictionary(dictionaryAssoc.getSource());				
			}
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 	
	}
	protected void removeModel(Entity entity) throws NodeException {
		TraversalQuery query = new TraversalQuery(TraversalQuery.ORDER_BREADTH_FIRST, TraversalQuery.DIRECTION_OUT, 1);
		query.getQNames().add(DictionaryModel.MODEL_FIELDS);
		query.getQNames().add(DictionaryModel.MODEL_FIELD_VALUES);
		query.getQNames().add(DictionaryModel.MODEL_RELATIONS);
		query.getQNames().add(DictionaryModel.MODEL_ASPECTS);							
		//getNodeService().removeNode(entity.getId(), query);
		//getEntityService().removeEntity(entity.getId());
	}
	@Override
	public void addUpdate(ModelObject object) {
		try	{
			if(object instanceof DataDictionary) {
				updateDataDictionary((DataDictionary)object);
			} else if(object instanceof Model) {
				updateModel((Model)object);				
			} else if(object instanceof ModelField)	{
				updateModelField((ModelField)object);
			} else if(object instanceof ModelRelation)	{
				updateModelRelation((ModelRelation)object);
			} else if(object instanceof ModelFieldValue)	{
				updateModelFieldValue((ModelFieldValue)object);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} 	
	}
	
	
	protected void updateDataDictionary(DataDictionary dictionary) throws DataDictionaryException {
		try	{
			Entity dictionaryEntity = (dictionary.getId() != null && dictionary.getId() > 0) ? entityService.getEntity(dictionary.getId()) : null;
			if(dictionaryEntity == null) dictionaryEntity = new EntityImpl(DictionaryModel.DICTIONARY);
			
			if(dictionary.getName() != null) {
				dictionaryEntity.setName(dictionary.getName());				
			}
			if(dictionary.getDescription() != null) {
				dictionaryEntity.addProperty(SystemModel.DESCRIPTION, dictionary.getDescription());
			}
			if(dictionary.getInheritance() != null) {
				dictionaryEntity.addProperty(DictionaryModel.INHERITANCE, dictionary.getInheritance());
			}
			if(dictionaryEntity.getId() != null && dictionaryEntity.getId() > 0) entityService.updateEntity(dictionaryEntity);
			else entityService.addEntity(dictionaryEntity);
			dictionary.setId(dictionaryEntity.getId());
			refreshDictionary(dictionaryEntity.getId());
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 
	}
	protected void updateModel(Model model) throws DataDictionaryException {
		try	{
			Entity modelEntity = (model.getId() != null && model.getId() > 0) ? entityService.getEntity(model.getId()) : null;
			if(modelEntity == null) {
				modelEntity = new EntityImpl(DictionaryModel.MODEL);
			}
			if(model.getQName() != null) {
				modelEntity.addProperty(DictionaryModel.QUALIFIED_NAME, model.getQName().toString());
			}
			if(model.getName() != null) {
				modelEntity.setName(model.getName());				
			}
			if(model.getDescription() != null) {
				modelEntity.addProperty(SystemModel.DESCRIPTION, model.getDescription());
			}
			if(model.getParent() != null) {
				modelEntity.addProperty(DictionaryModel.INHERITANCE, model.getParent().getQName().toString());
			}
			Entity dictionaryEntity = entityService.getEntity(model.getDictionary().getId());
			if(modelEntity.getId() != null && modelEntity.getId() > 0) {
				entityService.updateEntity(modelEntity);
			} else {
				if(model.getDictionary() == null || model.getDictionary().getId() == 0) throw new DataDictionaryException("no dictionary associated with this model");
				if(dictionaryEntity != null) {
					entityService.addEntity(modelEntity);
					Association assoc = new AssociationImpl(DictionaryModel.MODELS, dictionaryEntity.getId(), modelEntity.getId());					
					entityService.addAssociation(assoc);
				} else throw new DataDictionaryException("no dictionary associated with this model");
			}
			model.setId(modelEntity.getId());
			refreshDictionary(model.getDictionary().getId());
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		}  	
	}
	protected void updateModelField(ModelField field) throws DataDictionaryException {
		try	{
			Entity fieldEntity = (field.getId() != null && field.getId() > 0) ? entityService.getEntity(field.getId()) : null;
			if(fieldEntity == null) {
				fieldEntity = new EntityImpl(DictionaryModel.MODEL_FIELD);
			}
			if(fieldEntity != null) {
				if(field.getName() != null) {
					fieldEntity.setName(field.getName());			
				}
				if(field.getDescription() != null) {
					fieldEntity.addProperty(SystemModel.DESCRIPTION, field.getDescription());
				}				
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"mandatory"), field.isMandatory());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"unique"), field.isUnique());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"min_size"), field.getMinSize());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"max_size"), field.getMaxSize());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"format"), field.getFormat());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"type"), field.getType());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"qualified_name"), field.getQName().toString());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"order"), field.getOrder());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"index"), field.getIndex());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"sort"), field.getSort());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"hidden"), field.isHidden());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"label"), field.getLabel());
				fieldEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"tokenized"), field.isTokenized());
								
				if(field.getModel() == null || field.getModel().getId() == 0) throw new DataDictionaryException("no model associated with this field");
				Entity modelEntity = entityService.getEntity(field.getModel().getId());
				if(fieldEntity.getId() != null && fieldEntity.getId() > 0) {
					entityService.updateEntity(fieldEntity);
				} else {
					if(modelEntity != null) {
						entityService.addEntity(fieldEntity);
						Association assoc = new AssociationImpl(DictionaryModel.MODEL_FIELDS, modelEntity.getId(), fieldEntity.getId());
						entityService.addAssociation(assoc);
					} else throw new DataDictionaryException("no model associated with this field");
				}
				for(ModelFieldValue value : field.getValues()) {
					value.setField(field);
					updateModelFieldValue(value);
				}
			}
			field.setId(fieldEntity.getId());
			long dictionaryId = field.getModel().getDictionary().getId();
			refreshDictionary(dictionaryId);
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 		
	}
	protected void updateModelRelation(ModelRelation relation) throws DataDictionaryException {
		try	{
			assert(relation.getStartName() != null);
			assert(relation.getEndName() != null);
			assert(relation.getQName() != null);
			Entity relationEntity = (relation.getId() != null && relation.getId() > 0) ? entityService.getEntity(relation.getId()) : null;
			if(relationEntity == null) {
				relationEntity = new EntityImpl(DictionaryModel.MODEL_RELATION);
			}			
			if(relation.getName() != null) {
				relationEntity.setName(relation.getName());				
			}
			if(relation.getDescription() != null) {
				relationEntity.addProperty(SystemModel.DESCRIPTION, relation.getDescription());
			}			
			relationEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"qualified_name"), relation.getQName().toString());
			relationEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"many"), relation.isMany());
			relationEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"cascade"), relation.isCascade());
			
			relationEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"start"), relation.getStartName().toString());
			relationEntity.addProperty(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE,"end"), relation.getEndName().toString());
			
			if(relationEntity.getId() != null && relationEntity.getId() > 0) entityService.updateEntity(relationEntity);
			else {
				if(relation.getModel() == null || relation.getModel().getId() == 0) throw new DataDictionaryException("no model associated with this relation");
				Entity modelEntity = entityService.getEntity(relation.getModel().getId());
				if(modelEntity != null) {
					entityService.addEntity(relationEntity);
					Association assoc = new AssociationImpl(DictionaryModel.MODEL_RELATIONS, modelEntity.getId(), relationEntity.getId());
					entityService.addAssociation(assoc);
				} else throw new DataDictionaryException("no dictionary associated with this model");
				relation.setId(relationEntity.getId());
			}			
			long dictionaryId = relation.getModel().getDictionary().getId();
			refreshDictionary(dictionaryId);
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 		
	}
	protected void updateModelFieldValue(ModelFieldValue value) throws DataDictionaryException {
		try	{
			DataDictionaryUtility modelUtility = new DataDictionaryUtility(getEntityService());
			Entity valueEntity = (value.getId() != null && value.getId() > 0) ? entityService.getEntity(value.getId()) : null;
			if(valueEntity == null) {
				valueEntity = new EntityImpl(DictionaryModel.MODEL_FIELD_VALUE);
			}			
			valueEntity.setName(value.getName());
			if(value.getValue() != null)
				valueEntity.addProperty(SystemModel.VALUE, value.getValue());
			else
				valueEntity.addProperty(SystemModel.VALUE, value.getName());
				
			if(valueEntity.getId() != null && valueEntity.getId() > 0) entityService.updateEntity(valueEntity);
			else {
				if(value.getField() == null || value.getField().getId() == 0) throw new DataDictionaryException("no field associated with this value");
				Entity fieldEntity = entityService.getEntity(value.getField().getId());
				if(fieldEntity != null) {
					entityService.addEntity(valueEntity);
					Association assoc = new AssociationImpl(DictionaryModel.MODEL_FIELD_VALUES, fieldEntity.getId(), valueEntity.getId());
					entityService.addAssociation(assoc);
				} else throw new DataDictionaryException("no field associated with this value");
			}
			value.setId(valueEntity.getId());
			ModelField field = modelUtility.getModelField(value.getField().getId());
			long dictionaryId = field.getModel().getDictionary().getId();
			refreshDictionary(dictionaryId);
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 
	}
	
	
	protected void refreshDictionary(long id) throws InvalidEntityException {
		Entity entity = entityService.getEntity(id);
		DataDictionary dictionary = getDataDictionary(entity);
		userDictionaries.put(id, dictionary);
		System.out.println("refreshed " +dictionary.getName() +" dictionary");
	}
	
	protected void addUpdateModelParent(Model model) {
		try	{
			if(model.getId() > 0) {
				//if(model.getParent() != null) getNodeService().addUpdateNodeProperty(model.getId(), "parent", model.getParent());
				//else getNodeService().removeNodeProperty(model.getId(), "parent");
			}
			initialize();
		} catch(Exception e) {
			e.printStackTrace();
		} 	
	}
	/*
	protected void addUpdateModelFieldAspect(ModelFieldAspect aspect) throws DataDictionaryException {
		try	{
			Long node = null;
			if(aspect.getId() > 0) {
				node = aspect.getId();
			} else {				
				node = getNodeService().createNode(null);
				long fieldNode = aspect.getFieldId();
				getNodeService().addUpdateRelationship(DictionaryModel.MODEL_ASPECTS.toString(), fieldNode, node);
			}
			if(node != null) {				
				getNodeService().addUpdateNodeProperty(node, "qname", aspect.getQName().toString());
				getNodeService().addUpdateNodeProperty(node, "description", aspect.getDescription());
				//for(ModelField field : aspect.getFields()) {
					//field.setModel(node);
					//update(field);
				//}
			}
			aspect.setId(node);
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 		
	}
	*/
	public DataDictionary getSystemDictionary() {
		return systemDictionary;
	}
	public void setSystemDictionary(DataDictionary systemDictionary) {
		this.systemDictionary = systemDictionary;
	}
	public List<String> getSystemImports() {
		return systemImports;
	}
	public void setSystemImports(List<String> systemImports) {
		this.systemImports = systemImports;
	}
	public List<String> getBaseImports() {
		return baseImports;
	}
	public void setBaseImports(List<String> baseImports) {
		this.baseImports = baseImports;
	}
	public PropertyService getPropertyService() {
		return propertyService;
	}
	public void setPropertyService(PropertyService propertyService) {
		this.propertyService = propertyService;
	}

}
