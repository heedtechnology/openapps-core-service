package org.heed.openapps.dictionary.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.heed.openapps.InvalidQualifiedNameException;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.dictionary.DataDictionaryException;
import org.heed.openapps.dictionary.DictionaryModel;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelFieldSorter;
import org.heed.openapps.dictionary.ModelFieldValue;
import org.heed.openapps.dictionary.ModelObject;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.node.Direction;


public class DataDictionaryUtility {
	protected EntityService entityService;
	
	private Map<Long,ModelObject> objects = new HashMap<Long,ModelObject>();
	
	protected ModelFieldSorter modelFieldSorter = new ModelFieldSorter();
	
	
	public DataDictionaryUtility(EntityService entityService) {
		this.entityService = entityService;
	}
	
	public Model getModel(QName qname, long nodeId) throws DataDictionaryException {
		if(nodeId == 0) return null;
		try {
			Entity modelEntity = entityService.getEntity(nodeId);
			//QName qname = getQName(modelEntity);
			Model model = new Model(nodeId, qname);
			model.setName(modelEntity.getName());			
			model.setUid(modelEntity.getUid());
			
			Property prop = modelEntity.getProperty(SystemModel.DESCRIPTION.toString());
			if(prop != null) model.setDescription((String)prop.getValue());
			
			prop = modelEntity.getProperty(DictionaryModel.INHERITANCE);
			if(prop != null && !prop.getValue().equals("")) {
				QName parentQname = QName.createQualifiedName((String)prop.getValue());
				model.setParentName(parentQname);
			}
			
			List<Association> fieldAssocs = modelEntity.getAssociations(DictionaryModel.MODEL_FIELDS);
			for(Association rel : fieldAssocs) {
				Entity fieldEntity = entityService.getEntity(rel.getTarget());
				ModelField field = getModelField(fieldEntity);
				field.setModel(model);
				model.getFields().add(field);
				objects.put(field.getId(), field);
			}
			List<Association> relationAssocs = modelEntity.getSourceAssociations(DictionaryModel.MODEL_RELATIONS);
			for(Association rel : relationAssocs) {
				Entity fieldEntity = entityService.getEntity(rel.getTarget());
				ModelRelation field = getModelRelation(fieldEntity, Direction.OUTGOING);
				field.setModel(model);
				model.getSourceRelations().add(field);
				objects.put(field.getId(), field);
			}
			relationAssocs = modelEntity.getTargetAssociations(DictionaryModel.MODEL_RELATIONS);
			for(Association rel : relationAssocs) {
				Entity fieldEntity = entityService.getEntity(rel.getSource());
				ModelRelation field = getModelRelation(fieldEntity, Direction.INCOMING);
				field.setModel(model);
				model.getTargetRelations().add(field);
				objects.put(field.getId(), field);
			}
			Collections.sort(model.getFields(), modelFieldSorter);			
			return model;
		} catch(Exception e) {
			throw new DataDictionaryException("", e);
		} 	
	}
	public ModelField getModelField(long id) throws DataDictionaryException {
		ModelObject obj = objects.get(id);
		if(obj != null && obj instanceof ModelField)
			return (ModelField)obj;
		else throw new DataDictionaryException("no model field found for id:" + id);
	}
	protected ModelField getModelField(Entity fieldEntity) throws InvalidQualifiedNameException {
		try {						
			ModelField property = new ModelField(fieldEntity.getId(), getQName(fieldEntity));
			property.setName(fieldEntity.getName());
			Property prop = fieldEntity.getProperty(SystemModel.OPENAPPS_SYSTEM_NAMESPACE + "_description");
			if(prop != null && !prop.getValue().equals("")) property.setDescription((String)prop.getValue());
			prop = fieldEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_order");
			if(prop != null && !prop.getValue().equals("")) property.setOrder((Integer)prop.getValue());
			prop = fieldEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_type");
			if(prop != null && !prop.getValue().equals("")) property.setType((String)prop.getValue());
			prop = fieldEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_mandatory");
			if(prop != null && !prop.getValue().equals("")) property.setMandatory((Boolean)prop.getValue());
			prop = fieldEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_unique");
			if(prop != null && !prop.getValue().equals("")) property.setUnique((Boolean)prop.getValue());
			prop = fieldEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_min_size");
			if(prop != null && !prop.getValue().equals("")) property.setMinSize((Integer)prop.getValue());
			prop = fieldEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_max_size");
			if(prop != null && !prop.getValue().equals("")) property.setMaxSize((Integer)prop.getValue());
			prop = fieldEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_format");
			if(prop != null && !prop.getValue().equals("")) property.setFormat((Integer)prop.getValue());
			prop = fieldEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_index");
			if(prop != null && !prop.getValue().equals("")) property.setIndex((Integer)prop.getValue());
			prop = fieldEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_sort");
			if(prop != null && !prop.getValue().equals("")) property.setSort((Integer)prop.getValue());
			
			List<ModelFieldValue> values = new ArrayList<ModelFieldValue>();
			List<Association> valueAssociations = fieldEntity.getSourceAssociations(DictionaryModel.MODEL_FIELD_VALUES);
			for(Association valueRel : valueAssociations) {
				Entity valueEntity = entityService.getEntity(valueRel.getTarget());
				String description = valueEntity.getPropertyValue(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE, "value"));
				ModelFieldValue value = new ModelFieldValue(valueRel.getTarget(), valueEntity.getName(), description);
				value.setField(property);
				values.add(value);
			}
			property.setValues(values);
			return property;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	protected ModelRelation getModelRelation(Entity relationshipEntity, Direction direction) {		
		ModelRelation rel = null;
		try {
			QName qname = getQName(relationshipEntity);
			QName startName = relationshipEntity.hasProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_start") ? QName.createQualifiedName(relationshipEntity.getPropertyValue(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE, "start"))) : null;
			QName endName = relationshipEntity.hasProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_end") ? QName.createQualifiedName(relationshipEntity.getPropertyValue(new QName(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE, "end"))) : null;
			if(direction.equals(Direction.OUTGOING)) {				
				rel = new ModelRelation(relationshipEntity.getId(), startName, endName, ModelRelation.DIRECTION_OUTGOING, qname);
			} else {
				rel = new ModelRelation(relationshipEntity.getId(), endName, startName, ModelRelation.DIRECTION_INCOMING, qname);
			}
			rel.setName(relationshipEntity.getName());
			Property prop = relationshipEntity.getProperty(SystemModel.OPENAPPS_SYSTEM_NAMESPACE + "_description");
			if(prop != null && !prop.getValue().equals("")) rel.setDescription((String)prop.getValue());
			prop = relationshipEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_many");
			if(prop != null && !prop.getValue().equals("")) rel.setMany((Boolean)prop.getValue());
			prop = relationshipEntity.getProperty(DictionaryModel.OPENAPPS_SYSTEM_NAMESPACE + "_cascade");
			if(prop != null && !prop.getValue().equals("")) rel.setCascade((Boolean)prop.getValue());
			
			rel.setStartName(startName);
			rel.setEndName(endName);
			
			List<Association> valueAssociations = relationshipEntity.getSourceAssociations(DictionaryModel.MODEL_FIELDS);
			for(Association valueRel : valueAssociations) {
				Entity fieldEntity = entityService.getEntity(valueRel.getTarget());
				ModelField field = getModelField(fieldEntity);
				rel.getFields().add(field);
			}
			if(rel.getId() == 0) 
				throw new Exception("ModelRelation without id of type : "+qname);
		} catch(Exception e) {
			e.printStackTrace();
		}
		//if(rel !=  null) cacheService.put("dictionaryCache", rel.getUid(), rel);
		return rel;
	}
	/*
	protected ModelFieldAspect getModelFieldAspect(long node) throws InvalidQualifiedNameException {
		try {
			ModelFieldAspect aspect = new ModelFieldAspect(node, getQName(node));
			for(Relationship rel : nodeService.getRelationships(node, Direction.OUTGOING)) {
				if(rel.getQName().equals(DictionaryModel.MODEL_FIELDS)) {
					aspect.getFields().add(getModelField(rel.getEndNode()));
				} 
			}
			//cacheService.put("dictionaryCache", aspect.getUid(), aspect);
			return aspect;
		} catch(Exception e) {
			loggingService.error(e);
			return null;
		}
	}
	*/
	protected ModelFieldValue getModelFieldValue(Entity entity) {
		ModelFieldValue value = null;
		try {
			value = new ModelFieldValue(entity.getId(), entity.getName(), null);
			Property prop = entity.getProperty(SystemModel.OPENAPPS_SYSTEM_NAMESPACE + "_description");
			if(prop != null && !prop.getValue().equals("")) value.setDescription((String)prop.getValue());
		} catch(Exception e) {
			e.printStackTrace();
		}
		return value;
	}
	protected QName getQName(Entity entity) throws InvalidQualifiedNameException {
		QName qname = null;
		String qnameStr = "unknown";
		try {
			qnameStr = entity.getPropertyValue(DictionaryModel.QUALIFIED_NAME);
			if(qnameStr == null) throw new InvalidQualifiedNameException("missing qname property on node: "+entity.getId());
			qname = QName.createQualifiedName(qnameStr);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return qname;
	}
}
