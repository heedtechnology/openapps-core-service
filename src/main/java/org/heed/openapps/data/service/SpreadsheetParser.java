package org.heed.openapps.data.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.heed.openapps.data.SpreadsheetRecord;


public class SpreadsheetParser {

	
	public static List<SpreadsheetRecord> parse(byte[] content) throws IOException {
		List<SpreadsheetRecord> data = new ArrayList<SpreadsheetRecord>();
		Reader in = new InputStreamReader(new ByteArrayInputStream(content));
		int columnCount = 0;
		Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);					
		for(CSVRecord record : records) {
			SpreadsheetRecord row = new SpreadsheetRecord();
		    Iterator<String> iter = record.iterator();
		    int col = 0;
		    while(iter.hasNext()) {
				String value = String.valueOf(iter.next());
				if(col == 0) row.setA(value);
				if(col == 1) row.setB(value);
				if(col == 2) row.setC(value);
				if(col == 3) row.setD(value);
				if(col == 4) row.setE(value);
				if(col == 5) row.setF(value);
				if(col == 6) row.setG(value);
				if(col == 7) row.setH(value);
				if(col == 8) row.setI(value);
				if(col == 9) row.setJ(value);
				if(col == 10) row.setK(value);
				if(col == 11) row.setL(value);
				if(col == 12) row.setM(value);
				if(col == 13) row.setN(value);
				if(col == 14) row.setO(value);
				if(col == 15) row.setP(value);
				if(col == 16) row.setQ(value);
				if(col == 17) row.setR(value);
				if(col == 18) row.setS(value);
				if(col == 19) row.setT(value);
				if(col == 20) row.setU(value);
				if(col == 21) row.setV(value);
				if(col == 22) row.setW(value);
				if(col == 23) row.setX(value);
				if(col == 24) row.setY(value);
				if(col == 25) row.setZ(value);
				col++;
			}
		    if(columnCount < col) columnCount = col;
		    data.add(row);
		}
		return data;
	}
}
