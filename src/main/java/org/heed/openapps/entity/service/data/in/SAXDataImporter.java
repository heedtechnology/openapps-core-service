package org.heed.openapps.entity.service.data.in;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.AssociationImpl;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityImpl;
import org.heed.openapps.entity.Property;
import org.heed.openapps.util.XMLUtility;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXDataImporter {
	private List<Entity> entities = new ArrayList<Entity>();
	private List<Association> associations = new ArrayList<Association>();
	
	public void process(InputStream stream) throws Exception {
		XMLUtility.SAXParse(false, stream, new ImportHandler());
		for(Association assoc : associations) {
			String idAttr = assoc.getTargetUid();
			for(Entity entity : entities) {
				if(entity.getUid().equals(idAttr)) {
					assoc.setTargetEntity(entity);
					assoc.setTargetUid(null);
				}
			}
		}
		for(Entity entity : entities) {
			entity.setUid(java.util.UUID.randomUUID().toString());
		}
	}
	public List<Entity> getEntities() {
		return entities;
	}
		
	public class ImportHandler extends DefaultHandler {
		StringBuffer buff = new StringBuffer();
		Map<String,String> namespaces = new HashMap<String,String>();
		Property property;
		
		public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException	{
			if(qName.equals("namespace") || qName.equals("import")) {
				namespaces.put(attrs.getValue("prefix"), attrs.getValue("uri"));
			} else if(qName.equals("entity")) {
				String nameAttr =  attrs.getValue("name");
				String idAttr = attrs.getValue("id");
				if(nameAttr != null && idAttr != null) {
					String[] name = nameAttr.split(":");
					String namespace = namespaces.get(name[0]);
					Entity entity = new EntityImpl();
					entity.setQName(new QName(namespace, name[1]));
					entity.setUid(idAttr);
					entities.add(entity);
				}
			} else if(qName.equals("property")) {
				String nameAttr =  attrs.getValue("name");
				String typeAttr =  attrs.getValue("type");
				if(nameAttr != null) {
					String[] name = nameAttr.split(":");
					String namespace = namespaces.get(name[0]);
					property = new Property(new QName(namespace, name[1]), typeAttr, null);
					entities.get(entities.size()-1).getProperties().add(property);
				}
			} else if(qName.equals("association")) {
				String nameAttr =  attrs.getValue("name");
				String idAttr =  attrs.getValue("id");
				if(nameAttr != null && idAttr != null) {
					String[] name = nameAttr.split(":");
					String namespace = namespaces.get(name[0]);
					Association association = new AssociationImpl(new QName(namespace, name[1]));
					association.setSourceEntity(entities.get(entities.size()-1));
					association.setTargetUid(idAttr);
					entities.get(entities.size()-1).getSourceAssociations().add(association);
					associations.add(association);
				}
			}
		}
	
		public void characters(char[] ch, int start, int length) throws SAXException {
			buff.append(ch,start,length);
		}
	
		public void endElement(String namespaceURI, String sName, String qName) throws SAXException	{
			String data = buff.toString().trim();
			if(qName.equals("property")) {
				try {
					property.setValue(data);
				} catch(Exception e) {
					e.printStackTrace();
				}
			} 
			buff = new StringBuffer();
		}
		
	}
}
