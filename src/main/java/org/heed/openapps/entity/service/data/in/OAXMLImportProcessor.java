package org.heed.openapps.entity.service.data.in;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.AssociationImpl;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityImpl;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.data.FileImportProcessor;
import org.heed.openapps.util.XMLUtility;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class OAXMLImportProcessor extends FileImportProcessor {
	private static final long serialVersionUID = -5648783055422828169L;
	private List<Association> associations = new ArrayList<Association>();
	
	
	public OAXMLImportProcessor(String id, String name) {
		super(id, name);
	}
	
	public void process(InputStream stream, Map<String, Object> properties) throws Exception {
		//XMLUtility.SAXParse(false, new UnicodeInputStream(stream, "UTF-8"), new ImportHandler());
		XMLUtility.SAXParse(false, stream, new ImportHandler());
		for(Association assoc : associations) {
			String idAttr = assoc.getTargetUid();
			for(Entity entity : getEntities().values()) {
				if(entity.getUid().equals(idAttr)) {
					assoc.setTargetEntity(entity);
					//assoc.setTargetUid(null);
					entity.getTargetAssociations().add(assoc);
				}
			}
		}
	}
			
	public class ImportHandler extends DefaultHandler {
		StringBuffer buff = new StringBuffer();
		Map<String,String> namespaces = new HashMap<String,String>();
		Entity entity;
		Property property;
		
		public void startElement(String namespaceURI, String sName, String qName, Attributes attrs) throws SAXException	{
			if(qName.equals("namespace") || qName.equals("import")) {
				namespaces.put(attrs.getValue("prefix"), attrs.getValue("uri"));
			} else if(qName.equals("entity")) {
				String nameAttr =  attrs.getValue("name");
				String idAttr = attrs.getValue("id");
				if(nameAttr != null && idAttr != null) {
					String[] name = nameAttr.split(":");
					String namespace = namespaces.get(name[0]);
					entity = new EntityImpl();
					entity.setQName(new QName(namespace, name[1]));
					entity.setUid(java.util.UUID.randomUUID().toString());
					entity.setId(Long.valueOf(idAttr));
					getEntities().put(entity.getUid(), entity);
				}
			} else if(qName.equals("property")) {
				String nameAttr =  attrs.getValue("name");
				String typeAttr =  attrs.getValue("type");
				if(nameAttr != null) {
					String[] name = nameAttr.split(":");
					String namespace = namespaces.get(name[0]);
					property = new Property(new QName(namespace, name[1]), typeAttr, null);
					if(typeAttr != null) {
						if(typeAttr.equals("date")) property.setType(ModelField.TYPE_DATE);
					}
					entity.getProperties().add(property);
				}
			} else if(qName.equals("association")) {
				String nameAttr =  attrs.getValue("name");
				String idAttr =  attrs.getValue("id");
				String cascadeAttr = attrs.getValue("cascade");
				if(nameAttr != null && idAttr != null) {
					String[] name = nameAttr.split(":");
					String namespace = namespaces.get(name[0]);
					Association association = new AssociationImpl(new QName(namespace, name[1]));
					association.setSourceEntity(entity);
					association.setTargetUid(idAttr);
					if(cascadeAttr != null && cascadeAttr.equals("true")) association.setCascades(true);
					entity.getSourceAssociations().add(association);
					associations.add(association);
				}
			}
		}
	
		public void characters(char[] ch, int start, int length) throws SAXException {
			buff.append(ch,start,length);
		}
	
		public void endElement(String namespaceURI, String sName, String qName) throws SAXException	{
			String data = buff.toString().trim();
			if(qName.equals("property")) {
				try {
					property.setValue(data);
				} catch(Exception e) {
					
				}
			} 
			buff = new StringBuffer();
		}
		
	}
}
