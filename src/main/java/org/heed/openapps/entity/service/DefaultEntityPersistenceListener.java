package org.heed.openapps.entity.service;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.SystemModel;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityPersistenceListener;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.indexing.IndexField;
import org.heed.openapps.scheduling.SchedulingService;
import org.heed.openapps.search.SearchService;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.util.HTMLUtility;


public class DefaultEntityPersistenceListener implements EntityPersistenceListener {
	protected EntityService entityService;
	protected DataDictionaryService dictionaryService;
	protected SecurityService securityService;
	protected SchedulingService schedulingService;
	protected SearchService searchService;
	protected String hash = "SHA-1";
	
	
	@Override
	public List<IndexField> index(Entity entity) {
		List<IndexField> fields = new ArrayList<IndexField>();
		try {
			Model model = dictionaryService.getSystemDictionary().getModel(entity.getQName());
			if(model != null && model.isSearchable()) {
				//log.info("adding entity of type:"+entity.getQName().toString()+" to equity indexing queue.");
				//Map<String,Object> properties = nodeService.getNodeProperties(entity.getId());
    			//properties.put("xid", entity.getXid());
    			for(Property property : entity.getProperties()) {
    				ModelField modelField = model.getField(property.getQName());
    				Object value = property.getValue();
    				if(property.getQName().equals(SystemModel.NAME)) {
    					if(value != null) {
    						value = HTMLUtility.removeTags(((String)value).replace(",", ""));
    					}
    				}
    				if(modelField != null)
    					fields.add(new IndexField(modelField.getType(), property.getQName().toString(), value, modelField.isSearchable()));
    				else
    					fields.add(new IndexField(ModelField.TYPE_SMALLTEXT, property.getQName().toString(), value, false));
    			}
    			List<Association> permissions = entity.getSourceAssociations(SystemModel.PERMISSIONS);
    			if(permissions.size() > 0) {
    				StringWriter writer = new StringWriter();
    				for(Association assoc : permissions) {
    					Entity target = getEntityService().getEntity(assoc.getTarget());
    					writer.append(target.getPropertyValue(SystemModel.NODE)+" ");
    				}
    				fields.add(new IndexField(ModelField.TYPE_SMALLTEXT, "acl", writer.toString().trim(), true));
    			}
    			//searchService.update(entity.getId(), properties);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return fields;
	}
	public void deindex(long id) {
		
	}
	
	@Override
	public void onAfterAdd(Entity entity) {
		/*
		Model model = dictionaryService.getSystemDictionary().getModel(entity.getQName());
		if(model.isEntityIndexed()) {
			entityService.index(entity.getId(), false);
		}
		*/
	}

	@Override
	public void onAfterAssociationAdd(Association association) {
		Model sourceModel = dictionaryService.getSystemDictionary().getModel(association.getSourceName());
		Model targetModel = dictionaryService.getSystemDictionary().getModel(association.getTargetName());
		if(sourceModel != null && sourceModel.isSearchable()) {
			//index(association.getSourceName(), association.getSource());
		}
		if(targetModel != null && targetModel.isSearchable()) {
			//index(association.getTargetName(), association.getTarget());
		}
	}

	@Override
	public void onAfterAssociationDelete(Association association) {
		Model sourceModel = dictionaryService.getSystemDictionary().getModel(association.getSourceName());
		Model targetModel = dictionaryService.getSystemDictionary().getModel(association.getTargetName());
		if(sourceModel != null && (sourceModel.isSearchable())) {
			//index(association.getSourceName(), association.getSource());
		}
		if(targetModel != null && (targetModel.isSearchable())) {
			//index(association.getTargetName(), association.getTarget());
		}
	}

	@Override
	public void onAfterAssociationUpdate(Association association) {
		Model sourceModel = dictionaryService.getSystemDictionary().getModel(association.getSourceName());
		Model targetModel = dictionaryService.getSystemDictionary().getModel(association.getTargetName());
		if(sourceModel.isSearchable()) {
			//index(association.getSourceName(), association.getSource());
		}
		if(targetModel.isSearchable()) {
			//index(association.getTargetName(), association.getTarget());
		}
	}

	@Override
	public void onAfterDelete(Entity entity) {
		Model model = dictionaryService.getSystemDictionary().getModel(entity.getQName());
		if(model.isSearchable()) {
			try {
				deindex(entity.getId());
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onAfterUpdate(Entity entity) {
		Model model = dictionaryService.getSystemDictionary().getModel(entity.getQName());
		if(model != null && model.isSearchable()) {
			//index(entity.getQName(), entity.getId());
		}
	}

	@Override
	public void onBeforeAdd(Entity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeAssociationAdd(Association association) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeAssociationDelete(Association association) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeAssociationUpdate(Association association) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeDelete(Entity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBeforeUpdate(Entity oldValue, Entity newValue) {
		// TODO Auto-generated method stub
		
	}

	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	public EntityService getEntityService() {
		return entityService;
	}
	public DataDictionaryService getDictionaryService() {
		return dictionaryService;
	}
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}
	public void setSchedulingService(SchedulingService schedulingService) {
		this.schedulingService = schedulingService;
	}
	public SchedulingService getSchedulingService() {
		return schedulingService;
	}
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	
}
