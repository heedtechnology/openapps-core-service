package org.heed.openapps.entity.service.data;
import java.util.List;
import java.util.Map;

import org.heed.openapps.QName;
import org.heed.openapps.node.Direction;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.node.Relationship;
import org.heed.openapps.node.Transaction;

public class QNameConversionService {
	private NodeService nodeService;
	private boolean cleanup = false;
	
	
	public void initialize() {
		try {
			List<Long> nodes = nodeService.getAllNodes();
			int nodeCount = 0;
			int propertyCount = 0;
			int cleanCount = 0;	
			int relCount = 0;
			int relPropertyCount = 0;
			int relCleanCount = 0;
			System.out.println("beginning QName node conversion for " +nodes.size()+ " nodes...");
			for(long id : nodes) {
				Transaction tx = nodeService.getTransaction();
				try {
					Map<String, Object> properties = nodeService.getNodeProperties(id);
					for(String key : properties.keySet()) {
						if(key.startsWith("{openapps.")) {
							String key2 = key.replace(".", "_").replace("{", "").replace("}", "_");
							Object value = properties.get(key);
							if(value instanceof String) {
								String valueStr = (String)value;
								if(valueStr.length() > 0) {
									if(cleanup()) {
										nodeService.removeNodeProperty(id, key);
										cleanCount++;
									}
									nodeService.addUpdateNodeProperty(id, key2, value);
									propertyCount++;
								} else {
									if(cleanup()) {
										nodeService.removeNodeProperty(id, key);
										cleanCount++;
									}
								}
							} else {
								if(cleanup()) {
									nodeService.removeNodeProperty(id, key);
									cleanCount++;
								}
								nodeService.addUpdateNodeProperty(id, key2, value);
								propertyCount++;
							}
						} else if(key.equals("qname")) {
							String value = (String)properties.get(key);
							String value2 = value.replace(".", "_").replace("{", "").replace("}", "_");
							if(cleanup()) {
								nodeService.removeNodeProperty(id, key);
								cleanCount++;
							}
							nodeService.addUpdateNodeProperty(id, key, value2);
						}
					}
					
					List<Relationship> relationships = nodeService.getRelationships(id, Direction.BOTH);
					for(Relationship relationship : relationships) {
						String relationshipType = relationship.getQName().toString();
						if(relationshipType.startsWith("openapps.") && relationship.getStartNode() > 0) {
							String relationshipType2 = relationshipType.replace(".", "_").replace("{", "").replace("}", "_");
							QName relationshipQName = QName.createQualifiedName(relationshipType2);
							Relationship newRelationship = nodeService.createRelationship(relationshipQName, relationship.getStartNode(), relationship.getEndNode());
							relCount++;							
							Map<String, Object> relationshipProperties = nodeService.getRelationshipProperties(relationship.getId());
							for(String key : relationshipProperties.keySet()) {
								if(key.startsWith("{openapps.")) {
									String key2 = key.replace(".", "_").replace("{", "").replace("}", "_");
									Object value = relationshipProperties.get(key);
									if(value instanceof String) {
										String valueStr = (String)value;
										if(valueStr.length() > 0) {
											if(cleanup()) {
												nodeService.removeRelationshipProperty(newRelationship.getId(), key2);
												relCleanCount++;
											}
											nodeService.addUpdateRelationshipProperty(newRelationship.getId(), key2, value);
											relPropertyCount++;
										} else {
											if(cleanup()) {
												nodeService.removeRelationshipProperty(newRelationship.getId(), key2);
												relCleanCount++;
											}
										}
									} else {
										if(cleanup()) {
											nodeService.removeRelationshipProperty(newRelationship.getId(), key2);
											relCleanCount++;
										}
										nodeService.addUpdateRelationshipProperty(newRelationship.getId(), key2, value);
										relPropertyCount++;
									}
								}								
							}							
							if(cleanup()) {
								nodeService.removeRelationship(relationship.getId());
							}							
						} 						
					}					
					tx.success();
					nodeCount++;
					if(nodeCount % 1000 == 0) {
						System.out.println("converted " +nodeCount+ " nodes and " +propertyCount+ " properties, cleaned up " +cleanCount+" node properties");
						System.out.println("converted " +relCount+ " relationships and " +relPropertyCount+ " properties, cleaned up " +relCleanCount+" relationship properties");
					}
				} catch(Exception e) {
					tx.failure();
					e.printStackTrace();
				} finally {
					tx.finish();
		        }					
			}
			System.out.println("converted " +nodeCount+ " nodes and " +propertyCount+ " properties, cleaned up " +cleanCount+" node properties");
			System.out.println("converted " +relCount+ " relationships and " +relPropertyCount+ " properties, cleaned up " +relCleanCount+" relationship properties");
		} catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("finished QName conversion service...");
	}

	public NodeService getNodeService() {
		return nodeService;
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	public boolean cleanup() {
		return cleanup;
	}
	public void setCleanup(boolean cleanup) {
		this.cleanup = cleanup;
	}	
}
