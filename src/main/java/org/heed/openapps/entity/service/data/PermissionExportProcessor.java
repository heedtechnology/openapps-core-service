package org.heed.openapps.entity.service.data;
import java.util.Map;

import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.DefaultExportProcessor;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.data.FormatInstructions;


public class PermissionExportProcessor extends DefaultExportProcessor {
	private static final long serialVersionUID = 4955815630883571562L;
	
	
	@Override
	public Map<String, Object> exportMap(FormatInstructions format, Association association) {
		Map<String, Object> map = super.exportMap(format, association);
		
		try {
			Entity target = format.printSources() ? association.getSourceEntity() : association.getTargetEntity();
			if(target == null) target = format.printSources() ? getEntityService().getEntity(association.getSource()) : getEntityService().getEntity(association.getTarget());
			if(target != null) {
				String target_id = target.getPropertyValue(SystemModel.PERMISSION_TARGET);
				if(target_id != null) {
					Entity target_entity = getEntityService().getEntity(Long.valueOf(target_id));
					map.put("node_qname", target_entity.getQName().toString());
					map.put("node_type", target_entity.getQName().getLocalName());
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return map;
	}

}
