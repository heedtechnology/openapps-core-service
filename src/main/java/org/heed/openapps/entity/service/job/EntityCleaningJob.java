package org.heed.openapps.entity.service.job;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.node.Direction;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.node.Relationship;
import org.heed.openapps.scheduling.JobSupport;


public class EntityCleaningJob extends JobSupport {
	private static final long serialVersionUID = 6599701636224829192L;
	protected NodeService nodeService;
	protected QName qname;
	
	public static final QName OPENAPPS_ENTITIES = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "entities");
	public static final QName OPENAPPS_ENTITY = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "entity");
	
	
	public EntityCleaningJob(NodeService nodeService, QName qname) {
		this.nodeService = nodeService;
		this.qname = qname;
		setLastMessage("preparing to clean entities");
	}
	
	@Override
	public void execute() {
		setLastMessage("starting entity cleaning job");
		/*
		for(int i=0; i < 50; i++) {
			setLastMessage("finished trip # "+i);
			try {
				Thread.sleep(1000);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}
		*/
		try {
			long entityNode = getEntityNode(qname);
			List<Long> nodes = new ArrayList<Long>();
			for(Relationship relationship : nodeService.getRelationships(entityNode, Direction.OUTGOING)) {
				String endName = (String)nodeService.getNodeProperty(relationship.getEndNode(), "qname");
				if(endName != null && !endName.equals(qname.toString())) {
					nodeService.removeRelationship(relationship.getId());
					nodes.add(relationship.getEndNode());
				}
			}
			int count = 0;
			for(Long node : nodes) {
				count++;
				try {
					String endName = (String)nodeService.getNodeProperty(node, "qname");
					QName endQName = QName.createQualifiedName(endName);
					//System.out.println(endName+" "+node.getId());				
					long entityNode2 = getEntityNode(endQName);
					nodeService.createRelationship(OPENAPPS_ENTITY, entityNode2, node);
					setLastMessage(count+" of "+nodes.size()+" re-parented");
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
			/*
			for(Node node : neo4jService.getAllNodes()) {
				if(node.hasProperty("qname")) {
					String q = (String)node.getProperty("qname");
					if(q != null) {
						try {
							if(QName.createQualifiedName(q).equals(qname)) {
								boolean match = false;
								for(Relationship relationship : node.getRelationships(Direction.INCOMING)) {
									Node start = relationship.getStartNode();
									String startQName = (String)start.getProperty("qname");
									if(startQName.equals(qname.toString())) match = true;
								}
								if(!match) System.out.println("no entity node for "+node.getId());
							}
						} catch(InvalidQualifiedNameException e) {
							e.printStackTrace();
						}
					}
				}
			}
			*/
		} catch(Exception e) {
			e.printStackTrace();
		} 
		setLastMessage("finished entity cleaning job");
		setComplete(true);
	}
	
	protected Long getEntityNode(QName qname) {
		try {
			Relationship modelsRelation = nodeService.getSingleRelationship(0, OPENAPPS_ENTITIES, Direction.OUTGOING);
			if(modelsRelation != null) {
				for(Relationship relation : nodeService.getRelationships(modelsRelation.getEndNode(), Direction.OUTGOING)) {
					String relQname = nodeService.hasNodeProperty(relation.getEndNode(), "qname") ? (String)nodeService.getNodeProperty(relation.getEndNode(), "qname") : "";
					if(relQname.equals(qname.toString()))
						return relation.getEndNode();				
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		} 
		return null;
	}	
}
