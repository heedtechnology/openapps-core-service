package org.heed.openapps.entity.service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityCleaningService;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;


public class EntityCleaningServiceImpl implements EntityCleaningService {
	private final static Logger log = Logger.getLogger(EntityCleaningServiceImpl.class.getName());
	private EntityService entityService;
	private EntityIdComparator comparator = new EntityIdComparator();
	
	public static final QName OPENAPPS_ENTITY = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "entity");
	
	
	@Override
	public void merge(Long[] ids, int mergeType) {
		log.info("cleaning started...");
		List<Entity> targets = new ArrayList<Entity>();
		try {
			for(Long id : ids) {
				targets.add(entityService.getEntity(id));
			}
			Collections.sort(targets, comparator);
			if(mergeType == EntityCleaningService.MERGE_LAST_TO_FIRST) {
				Collections.reverse(targets);
			}
			for(int i=0; i < targets.size()-1; i++) {
				Entity node1 = targets.get(i);
				Entity node2 = targets.get(i+1);
				log.info("merging node1:"+node1.getId()+" with node2:"+node2.getId());
				for(Property prop1 : node1.getProperties()) {
					Property prop2 = node2.getProperty(prop1.getQName());
					Object p1 = prop1.getValue();
					Object p2 = prop2 != null ? prop2.getValue() : null;
					if(!p1.equals(p2)) {
						if(!isEmpty(p1) && isEmpty(p2)) {
							node2.addProperty(prop1.getQName(), p1);
							log.info("adding property:"+prop1);
						} 									
					}
				} 
				/*
				List<Relationship> relationships = nodeService.getRelationships(node1.getId(), Direction.BOTH);
				for(Relationship rel1 : relationships) {
					boolean exists = false;
					if(!rel1.getQName().equals(OPENAPPS_ENTITY)) {
						List<Relationship> relationships2 = nodeService.getRelationships(node2.getId(), Direction.BOTH);
						for(Relationship rel2 : relationships2) {
							if(rel1.getQName().equals(rel2.getQName()) && 
								rel1.getStartNode() == rel2.getStartNode() &&
								rel1.getEndNode() == rel2.getEndNode()) {
								exists = true;
							}
						}
					}
					if(!exists) {
						if(rel1.getStartNode() == node1.getId())
							nodeService.createRelationship(rel1.getQName(), node2.getId(), rel1.getEndNode());
						else
							nodeService.createRelationship(rel1.getQName(), node2.getId(), rel1.getStartNode());
					}
				}
				*/
				log.info("deleting entity:"+node1.getId());
				entityService.removeEntity(node1.getQName(), node1.getId());
			}
		} catch(Exception e) {
			e.printStackTrace();
		} 
	}
	protected boolean isEmpty(Object value) {
		return (value == null || String.valueOf(value).length() == 0);
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
	
	public class EntityIdComparator implements Comparator<Entity> {
		@Override
		public int compare(Entity node1, Entity node2) {
			if(node1.getId() > node2.getId()) return 1;
			else if(node1.getId() < node2.getId()) return -1;
			return 0;
		}		
	}
}
