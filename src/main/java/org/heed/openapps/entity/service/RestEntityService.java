package org.heed.openapps.entity.service;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;

import org.heed.openapps.QName;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.AssociationImpl;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityImpl;
import org.heed.openapps.entity.EntityPersistenceListener;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.ImportProcessor;
import org.heed.openapps.entity.InvalidAssociationException;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.InvalidPropertyException;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.entity.service.data.in.OAXMLImportProcessor;
import org.heed.openapps.net.http.HttpResponse;
import org.heed.openapps.net.http.RestUtility;


public class RestEntityService implements EntityService {
	private static final long serialVersionUID = -3944314239471337911L;
	private String serviceUrl;
	private Map<String, List<ImportProcessor>> importers = new HashMap<String, List<ImportProcessor>>();
	private Map<String, ExportProcessor> exporters = new HashMap<String, ExportProcessor>();
	private Map<String, EntityPersistenceListener> persistenceListeners = new HashMap<String, EntityPersistenceListener>();
	
	private ImportProcessor defaultImportProcessor = new OAXMLImportProcessor("default", "OpenApps XML");
	
	
	public RestEntityService() {}
	public RestEntityService(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}	
	
	@Override
	public Association getAssociation(long id) throws InvalidAssociationException {
		Association association = null;
		try {
			HttpResponse response = RestUtility.httpGet(serviceUrl+"/service/association/get.json?id="+id, getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				for(int i=0; i < data.size(); i++) {
					JsonObject entityObj = data.getJsonObject(i);
					association = new AssociationImpl(entityObj);
				}
			}
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
		return association;
	}

	@Override
	public Association getAssociation(QName qname, long source, long target) throws InvalidAssociationException {
		Association association = null;
		try {
			HttpResponse response = RestUtility.httpGet(serviceUrl+"/service/association/get.json?qname="+qname.toString()+"&source="+source+"&target="+target, getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				for(int i=0; i < data.size(); i++) {
					JsonObject entityObj = data.getJsonObject(i);
					association = new AssociationImpl(entityObj);
				}
			}
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
		return association;
	}
	
	@Override
	public Entity getEntity(long id) throws InvalidEntityException {
		Entity entity = null;
		try {
			HttpResponse response = RestUtility.httpGet(serviceUrl+"/service/entity/get.json?id="+id, getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				for(int i=0; i < data.size(); i++) {
					JsonObject entityObj = data.getJsonObject(i);
					entity = new EntityImpl(entityObj);
				}
			}
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
		return entity;
	}
	
	@Override
	public Entity getEntity(QName qname, long id) throws InvalidEntityException {
		return getEntity(id);
	}
	
	public Entity getEntity(QName qname, Object value) throws InvalidEntityException {
		return null;
	}
	
	@Override
	public Entity getEntity(String uid) throws InvalidEntityException {
		Entity entity = null;
		try {
			HttpResponse response = RestUtility.httpGet(serviceUrl+"/service/entity/get.json?uid="+uid, getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				for(int i=0; i < data.size(); i++) {
					JsonObject entityObj = data.getJsonObject(i);
					entity = new EntityImpl(entityObj);
				}
			}
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
		return entity;
	}

	
	@Override
	public Long addAssociation(Association association) throws InvalidAssociationException {
		try {
			HttpResponse response = RestUtility.httpPost(serviceUrl+"/service/entity/association/add.json", association.toJsonObject().toString().getBytes("UTF-8"), getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				if(data.size() > 0) {
					JsonObject entityObj = data.getJsonObject(0);
					Association assoc =  new AssociationImpl(entityObj);
					return assoc.getId();
				}
			}
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
		return null;
	}

	@Override
	public void addEntities(Collection<Entity> entities) throws InvalidEntityException {
		try {
			HttpResponse response = RestUtility.httpPost(serviceUrl+"/service/entity/association/add.json", toJsonObject(entities).toString().getBytes("UTF-8"), getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				//JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        /*
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");				
		        for(int i=0; i < data.size(); i++) {
					JsonObject entityObj = data.getJsonObject(i);
					Association assoc =  new AssociationImpl(entityObj);
					return assoc.getId();
				}
				*/
			}
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
	}

	@Override
	public Long addEntity(Entity entity) throws InvalidEntityException {
		try {
			HttpResponse response = RestUtility.httpPost(serviceUrl+"/service/entity/entity/add.json", entity.toJsonObject().toString().getBytes("UTF-8"), getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				if(data.size() > 0) {
					JsonObject entityObj = data.getJsonObject(0);
					Entity assoc =  new EntityImpl(entityObj);
					return assoc.getId();
				}
			}
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
		return null;
	}

	@Override
	public Long addEntity(Long source, Long target, QName association, List<Property> associstionProperties, Entity entity) throws InvalidEntityException {
		try	{
			addEntity(entity);
			Association assoc = null;
			if(source != null) assoc = getAssociation(association, source, entity.getId());
			else if(target != null) assoc = getAssociation(association, entity.getId(), target);
			if(assoc != null) {
				if(associstionProperties != null) {
					for(Property property : associstionProperties) {
						assoc.getProperties().add(property);
					}
				}
				addAssociation(assoc);
				return assoc.getId();
			} else {
				throw new InvalidEntityException("invalid association source:"+source+" target:"+target);
			}			
		} catch(InvalidAssociationException e1) {
			e1.printStackTrace();
		} 
		return null;
	}

	@Override
	public void updateAssociation(Association association) throws InvalidAssociationException {
		try {
			HttpResponse response = RestUtility.httpPost(serviceUrl+"/service/entity/association/update.json", association.toJsonObject().toString().getBytes("UTF-8"), getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				//JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        /*
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				if(data.size() > 0) {
					JsonObject entityObj = data.getJsonObject(0);
					Association assoc =  new AssociationImpl(entityObj);
					
				}
				*/
			} else throw new InvalidAssociationException("error updating association :"+response.getStatusCode()+" "+response.getMessage());
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
	}

	@Override
	public void updateEntity(Entity entity) throws InvalidEntityException {
		try {
			HttpResponse response = RestUtility.httpPost(serviceUrl+"/service/entity/entity/update.json", entity.toJsonObject().toString().getBytes("UTF-8"), getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				//JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        /*
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				if(data.size() > 0) {
					JsonObject entityObj = data.getJsonObject(0);
					Entity assoc =  new EntityImpl(entityObj);
				}
				*/
			} else throw new InvalidEntityException("error updating entity :"+response.getStatusCode()+" "+response.getMessage());
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
	}
	
	@Override
	public void removeAssociation(long id) throws InvalidAssociationException {
		try {
			HttpResponse response = RestUtility.httpPost(serviceUrl+"/service/entity/association/remove.json?id="+id, new HashMap<String,String>(0), getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				//JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        /*
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				if(data.size() > 0) {
					JsonObject entityObj = data.getJsonObject(0);
					Entity assoc =  new EntityImpl(entityObj);
				}
				*/
			}
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
	}

	@Override
	public void removeEntity(QName qname, long id) throws InvalidEntityException {
		try {
			HttpResponse response = RestUtility.httpPost(serviceUrl+"/service/entity/entity/remove.json?qname="+qname.toString()+"&id="+id, new HashMap<String,String>(0), getHeaders());
			if(response.getStatusCode() == 200) {
				JsonReader jsonReader = Json.createReader(new StringReader(response.getContent()));
				//JsonObject jsonObject = jsonReader.readObject();
		        jsonReader.close();
		        /*
		        JsonObject json = (JsonObject)jsonObject.get("response");
		        JsonArray data = json.getJsonArray("data");
				if(data.size() > 0) {
					JsonObject entityObj = data.getJsonObject(0);
					Entity assoc =  new EntityImpl(entityObj);
				}
				*/
			}
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
	}
	
	@Override
	public void cascadeProperty(QName qname, Long id, QName association, QName propertyName, Serializable propertyValue) throws InvalidEntityException, InvalidPropertyException {
		try	{
			Entity entity = getEntity(id);
			List<Association> associations = entity.getSourceAssociations(association);
			for(Association assoc : associations) {
				Entity targetEntity = getEntity(assoc.getTarget());
				targetEntity.addProperty(propertyName, propertyValue);
				updateEntity(targetEntity);
				cascadeProperty(targetEntity.getQName(), targetEntity.getId(), association, propertyName, propertyValue);
			}
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
	}

	protected Map<String,String> getHeaders() {
		Map<String,String> headers = new HashMap<String,String>();
		headers.put("Content-Type", "application/json;charset=UTF-8");
		headers.put("Accept", "application/json;charset=UTF-8");
		
		return headers;
	}
	
	@Override
	public Object export(FormatInstructions arg0, Entity arg1) throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object export(FormatInstructions arg0, Association arg1) throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}
		
	public void registerImportProcessor(String name, ImportProcessor processor) {
		List<ImportProcessor> list = importers.get(name);
		if(list == null) {
			list = new ArrayList<ImportProcessor>();
			importers.put(name, list);
		}
		list.add(processor);
	}
	public void registerExportProcessor(String name, ExportProcessor processor) {
		exporters.put(name, processor);
	}
	public void registerEntityPersistenceListener(String name, EntityPersistenceListener component) {
		persistenceListeners.put(name, component);
	}
	@Override
	public List<ImportProcessor> getImportProcessors(String name) {
		List<ImportProcessor> processors = importers.get(name);
		if(processors == null) processors = new ArrayList<ImportProcessor>();
		if(!processors.contains(defaultImportProcessor)) processors.add(defaultImportProcessor);
		return processors;
	}
	@Override
	public ExportProcessor getExportProcessor(String name) {
		ExportProcessor processor = exporters.get(name);
		if(processor != null) return processor;
		else return exporters.get("default");
	}
	@Override
	public EntityPersistenceListener getEntityPersistenceListener(String name) {
		EntityPersistenceListener listener = persistenceListeners.get(name);
		if(listener == null ) listener = persistenceListeners.get("default");
		return listener;
	}
	public void setImporters(Map<String, List<ImportProcessor>> importers) {
		this.importers = importers;
	}
	public void addExporter(String name, ExportProcessor exporter) {
		exporters.put(name, exporter);
	}
	public void setExporters(Map<String, ExportProcessor> exporters) {
		this.exporters = exporters;
	}
	public void addPersistenceListener(String name, EntityPersistenceListener listener) {
		persistenceListeners.put(name, listener);
	}
	public void setPersistenceListeners(Map<String, EntityPersistenceListener> persistenceListeners) {
		this.persistenceListeners = persistenceListeners;
	}
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	protected JsonObject toJsonObject(Collection<Entity> entities) {
		JsonObjectBuilder builder = Json.createObjectBuilder();		
		JsonArrayBuilder dataBuilder = Json.createArrayBuilder();
		for(Entity obj : entities) {			
			dataBuilder.add(obj.toJsonObject());
		}
		builder.add("data", dataBuilder);		
		builder.add("totalRows", entities.size());
		return builder.build();
	}
	@Override
	public Entity getEntity(QName qname, QName propertyQname, Object value) throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public EntityResultSet getEntities(QName qname, int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public EntityResultSet getEntities(QName qname, QName propertyQname, Object value, int page, int size)
			throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int count(QName qname) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Entity getEntity(long id, boolean sources, boolean targets) throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public EntityResultSet getEntities(QName qname, QName propertyQname, int page, int size)
			throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}
	
}
