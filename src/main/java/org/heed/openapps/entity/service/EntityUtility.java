package org.heed.openapps.entity.service;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.heed.openapps.QName;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.AssociationImpl;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityImpl;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.InvalidAssociationException;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.ModelValidationException;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ValidationField;
import org.heed.openapps.entity.ValidationResult;
import org.heed.openapps.util.NumberUtility;

public class EntityUtility {
	private EntityService entityService;
	private DataDictionaryService dictionaryService;
	
	
	public EntityUtility(EntityService entityService, DataDictionaryService dictionaryService) {
		this.entityService = entityService;
		this.dictionaryService = dictionaryService;
	}
	
	public Entity getEntity(HttpServletRequest request, QName entityQname) throws InvalidEntityException {
		Entity entity = null;
		String id = request.getParameter("id");
		if(id != null && id.length() > 0) {
			entity = entityService.getEntity(Long.valueOf(id));
		} else {
			entity = new EntityImpl(entityQname);
			entity.setCreated(System.currentTimeMillis());
			entity.setModified(System.currentTimeMillis());
			String uid = request.getParameter("uid");
			if(uid != null && uid.length() > 0) entity.setUid(uid);
			else entity.setUid(UUID.randomUUID().toString());
		}
		if(entity != null) {
			try {
				String name = request.getParameter("name");
				if(name != null) entity.setName(name);
				DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
				List<ModelField> fields = dictionary.getModelFields(entity.getQName());
				for(ModelField field : fields) {
					try {
						String value = field.getQName().getLocalName().equals("function") ? 
							request.getParameter("_"+field.getQName().getLocalName()) : 
								request.getParameter(field.getQName().getLocalName());
						if(value != null && value.length() > 0 && !value.equals("null")) {
							if(field.getType() == ModelField.TYPE_DATE) {
								entity.addProperty(ModelField.TYPE_DATE, field.getQName(), value);
							}
							else if(field.getType() == ModelField.TYPE_INTEGER) {
								int val = Integer.valueOf(value);
								entity.addProperty(ModelField.TYPE_INTEGER, field.getQName(), val);
							}
							else if(field.getType() == ModelField.TYPE_LONG) {
								long val = Long.valueOf(value);
								entity.addProperty(ModelField.TYPE_LONG, field.getQName(), val);
							}
							else if(field.getType() == ModelField.TYPE_BOOLEAN) {
								boolean val = Boolean.valueOf(value);
								entity.addProperty(ModelField.TYPE_BOOLEAN, field.getQName(), val);
							}
							else entity.addProperty(field.getQName(), value);				
						} else {
							if(field.getType() == ModelField.TYPE_DATE) entity.addProperty(ModelField.TYPE_DATE, field.getQName(), value);
							else if(field.getType() == ModelField.TYPE_INTEGER) entity.addProperty(ModelField.TYPE_INTEGER, field.getQName(), 0);
							else if(field.getType() == ModelField.TYPE_LONG) entity.addProperty(ModelField.TYPE_LONG, field.getQName(), 0);
							else if(field.getType() == ModelField.TYPE_BOOLEAN) entity.addProperty(ModelField.TYPE_BOOLEAN, field.getQName(), false);
							else entity.addProperty(field.getQName(), value);
						}
					} catch(Exception e) {
						e.toString();
					}
				}
				for(ModelRelation relation : dictionary.getModelRelations(entity.getQName(), ModelRelation.DIRECTION_OUTGOING)) {
					String values = request.getParameter(relation.getQName().getLocalName());
					if(values != null) {
						String[] ids = values.replaceFirst("\\[", "").replaceAll("\\]", "").split(",");
						List<Association> assocs = entity.getAssociations(relation.getQName());
						for(String target_id : ids) {
							if(target_id.length() > 0) {
								boolean match = false;
								for(Association assoc : assocs) {
									if(assoc.getTarget().equals(Long.valueOf(target_id))) match = true;
								}
								if(!match) {						
									Entity target = entityService.getEntity(Long.valueOf(target_id));
									Association a = new AssociationImpl(relation.getQName(), entity, target);
									entity.getSourceAssociations().add(a);
								}
							}
						}
					}
				}			
			} catch(Exception e) {
				throw new InvalidEntityException("entity model not found for qname:"+entityQname.toString(), e);
			}
		}
		return entity;
	}
	public Association getAssociation(HttpServletRequest request, QName associationQname) throws InvalidAssociationException {
		Association association = null;
		String id = request.getParameter("id");
		String source = request.getParameter("source");
		String target = request.getParameter("target");
		if(id != null && id.length() > 0) {
			association = entityService.getAssociation(Long.valueOf(id));
		} else {			
			if(source == null) throw new InvalidAssociationException("association source is null for qname:"+associationQname.toString());
			if(target == null) throw new InvalidAssociationException("association target is null for qname:"+associationQname.toString());
			try {
				association = new AssociationImpl(associationQname);
				if(NumberUtility.isLong(source)) association.setSource(Long.valueOf(source)); 
				else association.setSourceUid(source);
				if(NumberUtility.isLong(target)) association.setTarget(Long.valueOf(target)); 
				else association.setTargetUid(target);
			} catch(Exception e) {
				throw new InvalidAssociationException("invalid entities source:"+source+" target:"+target, e);
			}
		}
		if(association == null) throw new InvalidAssociationException("association cannot be found for qname:"+associationQname.toString());
		try {
			DataDictionary dictionary = dictionaryService.getDataDictionary(association.getDictionary());
			Entity entity = entityService.getEntity(Long.valueOf(source));
			ModelRelation model = dictionary.getModelRelation(entity.getQName(), associationQname);
			if(model == null) throw new InvalidAssociationException("entity relation not found for qname:"+associationQname.toString());
			for(ModelField field : model.getFields()) {
				String value = request.getParameter(field.getQName().toString());
				if(value == null) {
					value = field.getQName().getLocalName().equals("function") ? 
						request.getParameter("_"+field.getQName().getLocalName()) : 
						request.getParameter(field.getQName().getLocalName());
				}
				if(value != null && !value.equals("null")) {
					/*
					if(field.getFormat() == ModelField.FORMAT_PASSWORD) {
						SimpleHash simpleHash = new SimpleHash(hash, value);
						value = simpleHash.toHex();
					}
					*/
					try {
						association.addProperty(field.getQName(), field.getType(), value);
					} catch(Exception e) {
						e.toString();
					}
				} else {
					if(association.hasProperty(field.getQName())) association.removeProperty(field.getQName());
				}
			}
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
		return association;
	}
	public ValidationResult validate(Entity entity) {
		ValidationResult result = new ValidationResult();
		try {
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			Model model = dictionary.getModel(entity.getQName());
			if(model == null) result.setException(ValidationResult.MODEL_MISSING);
			else {
				for(ModelField field : model.getFields()) {
					if(field.getQName() != null && field.getQName().getNamespace() != null && field.getQName().getLocalName() != null) {
						Property property = entity.getProperty(field.getQName().getNamespace(), field.getQName().getLocalName());
						if(property == null) property = entity.getProperty("", field.getQName().getLocalName());
						if(property != null) {
							if(property.getQName().getNamespace() == null) property.getQName().setNamespace(field.getQName().getNamespace());
							if(property.toString().length() < field.getMinSize()) result.addValidationField(new ValidationField(field.getQName().getLocalName(), ValidationField.MINIMUM_VALUE_RESTRICTION));
							else if(field.getMaxSize() > 0 && property.toString().length() > field.getMaxSize()) result.addValidationField(new ValidationField(field.getQName().getLocalName(), ValidationField.MAXIMUM_VALUE_RESTRICTION));
						} else if(field.isMandatory()) result.addValidationField(new ValidationField(field.getQName().getLocalName(), ValidationField.REQUIRED_VALUE_MISSING));
					}
				}
			}
		} catch(Exception e) {
			result.setValid(false);
			result.setMessage(e.getLocalizedMessage());
			System.out.println(result.toString());
		}
		return result;
	}
	public ValidationResult validate(Association association) throws ModelValidationException {
		DataDictionary dictionary = dictionaryService.getDataDictionary(association.getDictionary());
		ValidationResult result = new ValidationResult(true);
		if(association.getSource() == 0) {
			result.setValid(false);
			result.addValidationField(new ValidationField("source", ValidationResult.MISSING_VALUE));
		}
		if(association.getTarget() == 0) {
			result.setValid(false);
			result.addValidationField(new ValidationField("target", ValidationResult.MISSING_VALUE));
		}
		if(result.isValid()) {
			try {
				Model model = dictionary.getModel(association.getSourceName());
				Entity entity = entityService.getEntity(association.getTarget());
				if(model != null) {
					if(association.getQName() == null) {				
						for(ModelRelation rel : model.getRelations()) {
							if(rel.getEndName().equals(entity.getQName())) {
								association.setQname(rel.getQName());
								result.setValid(true);
								return result;
							}
						}
					} else {
						Model m = model;
						while(m != null) {
							if(m.containsRelation(association.getQName())) {
								result.setValid(true);
								return result;
							}
							m = dictionary.getModel(m.getParentName());
						}
					}
					result.setValid(false);
					result.setMessage("Association of type "+association.getQName()+" not found.");
				}
			} catch(Exception e) {
				throw new ModelValidationException(result);
			}
		}
		return result;
	}
}
