package org.heed.openapps.entity.service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.heed.openapps.InvalidQualifiedNameException;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.cache.CacheService;
import org.heed.openapps.dictionary.DataDictionary;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.dictionary.ModelRelation;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.AssociationImpl;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityImpl;
import org.heed.openapps.entity.EntityPersistenceListener;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.ImportProcessor;
import org.heed.openapps.entity.InvalidAssociationException;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.InvalidPropertyException;
import org.heed.openapps.entity.ModelValidationException;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ValidationField;
import org.heed.openapps.entity.ValidationResult;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.entity.service.data.in.OAXMLImportProcessor;
import org.heed.openapps.entity.service.job.EntityCleaningJob;
import org.heed.openapps.node.Direction;
import org.heed.openapps.node.Node;
import org.heed.openapps.node.NodeService;
import org.heed.openapps.node.Relationship;
import org.heed.openapps.node.Transaction;
import org.heed.openapps.scheduling.Job;
import org.heed.openapps.scheduling.SchedulingService;


public class NodeEntityService implements EntityService {
	private static final long serialVersionUID = 4922262363213062391L;
	private NodeService nodeService;
	private DataDictionaryService dictionaryService;
	private SchedulingService schedulingService;
	private CacheService cacheService;
	
	private Map<String, List<ImportProcessor>> importers = new HashMap<String, List<ImportProcessor>>();
	private Map<String, ExportProcessor> exporters = new HashMap<String, ExportProcessor>();
	private Map<String, EntityPersistenceListener> persistenceListeners = new HashMap<String, EntityPersistenceListener>();
	
	public boolean acceptEmptyValues = false;
	private boolean loggingOn = false;
	
	public static final QName OPENAPPS_DELETED = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "deleted");
	public static final QName OPENAPPS_ENTITIES = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "entities");
	public static final QName OPENAPPS_ENTITY = new QName(SystemModel.OPENAPPS_SYSTEM_NAMESPACE, "entity");
	
	private ImportProcessor defaultImportProcessor = new OAXMLImportProcessor("default", "OpenApps XML");
	//private AssociationSorter associationSorter = new AssociationSorter(new Sort(Sort.STRING, "openapps_org_system_1_0_name", false));
	
	//private IndexingWorkQueue indexingQueue;
	
	public void initialize() {

	}
	
	public Entity getEntity(String uid) throws InvalidEntityException {
		/*
		try {
			Long id = nodeService.getNodeId(uid);
			if(id != null) return getEntity(entityQname, id);
			else return null;//throw new InvalidEntityException("entity not found:"+uid);
		} catch(NodeException e) {
			throw new InvalidEntityException("", e);
		}
		*/
		return null;
	}
	
	public Entity getEntity(Entity entity) throws InvalidEntityException {
		try {
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			Model model = dictionary.getModel(entity.getQName());
			if(model != null) {
				String query = "";
				List<ModelField> uniqueFields = model.getUniqueFields(true);
				for(ModelField field : uniqueFields) {
					query += field.getQName().getLocalName()+":"+entity.getPropertyValue(field.getQName())+" AND ";
				}
				query = query.substring(0, query.length()-5);
				System.out.println("searching for entity : "+query);
				/*
				EntityQuery entityQuery = new EntityQuery(entity.getQName(), query);
				EntityResultSet<Entity> entities = search(entityQuery);
				if(entities != null && entities.getResultSize() > 0) {
					return entities.getResults().get(0);
				}
				*/
			}			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public Entity getEntity(String name, String value) throws InvalidEntityException {
		return null;
	}
	public Entity getEntity(long nodeId) throws InvalidEntityException {
		try {
			QName qname = nodeService.getNodeQName(nodeId);
			return getEntity(qname, nodeId);			
		} catch(Exception e) {
			throw new InvalidEntityException("", e);
		}
	}
	public Entity getEntity(QName qname, long nodeId) throws InvalidEntityException {
		Object entityObj = cacheService.get("entityCache", String.valueOf(nodeId));
		if(entityObj != null && entityObj instanceof Entity) return (Entity)entityObj;
		Entity entity = null;
		try {
			//QName qname = nodeService.getNodeQName(nodeId);
			//EntityPersistenceListener listener = persistenceListeners.get(qname.toString());		
			//if(listener != null) {
				//entity = listener.extractEntity(nodeId, qname);
			//}
			Node node = nodeService.getNode(nodeId);
			if(entity == null) entity = new EntityImpl(node);
			try {					
				DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
				List<ModelField> fields = dictionary.getModelFields(node.getQName());
				for(ModelField field : fields) {
					try {
						Object value = nodeService.hasNodeProperty(nodeId, field.getQName().toString()) ? nodeService.getNodeProperty(nodeId, field.getQName().toString()) : null;
						if(value != null && !value.equals("null")) {
							Property property = new Property(field.getQName(), field.getType(), null);
							if(field.getType() == ModelField.TYPE_SMALLTEXT) {
								property.setValue(value);
							} else if(field.getType() == ModelField.TYPE_LONGTEXT) {
								property.setValue(value);
							} else if(field.getType() == ModelField.TYPE_DATE) {
								property.setValue(value);
							} else if(field.getType() == ModelField.TYPE_INTEGER) {
								if(value instanceof Integer) property.setValue(value);
								else if(value instanceof String && !value.equals(""))
									property.setValue(Integer.valueOf((String)value));
							} else if(field.getType() == ModelField.TYPE_DOUBLE) {
								if(value instanceof Double) property.setValue(value);
								else if(value instanceof String && !value.equals(""))
									property.setValue(Double.valueOf((String)value));
							} else if(field.getType() == ModelField.TYPE_LONG) {
								if(value instanceof Long) property.setValue(value);
								else if(value instanceof String && !value.equals(""))
									property.setValue(Long.valueOf((String)value));
							} else if(field.getType() == ModelField.TYPE_BOOLEAN) {
								if(value instanceof Boolean) property.setValue(value);
								else if(value instanceof String && !value.equals("")) 
									property.setValue(Boolean.valueOf((String)value));
							} 
							if(property.getValue() != null)
								entity.getProperties().add(property);
							else
								System.out.println("null property added : "+field.getQName());
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			} catch(Exception e) {
				//log.error("problem locating model fields for qname:"+qname);
			}
			List<Relationship> sources = nodeService.getRelationships(nodeId, Direction.OUTGOING);
			Map<QName, Integer> collections = new HashMap<QName, Integer>();				
			for(Relationship rel : sources) {
				QName relQname = nodeService.getRelationshipQName(rel.getId());
				if(relQname != null) {
					Integer count = collections.get(relQname);
					if(count == null) count = 1;
					if(count <= 20) {
						try {
							Association assoc = getAssociation(rel);
							if(assoc != null) {
								entity.getSourceAssociations().add(assoc);
							}
							collections.put(relQname, count + 1);
						} catch(InvalidAssociationException e) {
							throw new InvalidEntityException(e.getMessage());
						}
					}
				}
			}
			List<Relationship> targets = nodeService.getRelationships(nodeId, Direction.INCOMING);
			for(Relationship rel : targets) {
				QName relQname = nodeService.getRelationshipQName(rel.getId());
				if(relQname != null) {
					Integer count = collections.get(relQname);
					if(count == null) count = 1;
					if(count <= 20) {
						try {
							Association assoc = getAssociation(rel);
							if(assoc != null) {
								entity.getTargetAssociations().add(assoc);					
							}
							collections.put(relQname, count + 1);
						} catch(InvalidAssociationException e) {
							throw new InvalidEntityException(e.getMessage());
						}
					}
				}
			}
			cacheService.put("entityCache", String.valueOf(entity.getId()), entity);
		
		} catch(Exception e) {
			cacheService.remove("entityCache", String.valueOf(nodeId));
			throw new InvalidEntityException("", e);
		}
		return entity;
	}
	
	@Override
	public Long addEntity(Entity entity) throws InvalidEntityException {
		try	{
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			if(entity.getQName() == null) throw new InvalidEntityException("entity model not found for missing qname");
			EntityPersistenceListener listener = persistenceListeners.get(entity.getQName().toString());
			if(listener == null) listener = persistenceListeners.get("default");
			if(listener != null) listener.onBeforeAdd(entity);
			long nodeId = (entity.getUid() != null && entity.getUid().length() > 0) ? nodeService.createNode(entity.getUid()) : nodeService.createNode(null);			
			if(nodeId > 0) {
				entity.setId(nodeId);
				if(entity.getUid() == null || entity.getUid().length() == 0)
					entity.setUid(java.util.UUID.randomUUID().toString());
				nodeService.addUpdateNode(entity.getNode());			
				List<ModelField> fields = dictionary.getModelFields(entity.getQName());
				for(Property property : entity.getProperties()) {
					ModelField field = null;
					for(ModelField f : fields) {
						if(f.getQName().equals(property.getQName())) {
							field = f;
							break;
						}
					}						
					if(field != null) {							
						if(property.getValue() != null) {
							nodeService.addUpdateNodeProperty(entity.getId(), property.getQName().toString(), property.getValue());
						}
					} else if(property.getType().equals(ModelField.TYPE_COMPUTED)) {
						nodeService.addUpdateNodeProperty(entity.getId(), property.getQName().toString(), property.getValue());
					} else 
						if(loggingOn)
							System.out.println("Model field not found for : " + property.getQName().toString());
				}
				/*
				List<Association> sourceAssociations = entity.getSourceAssociations();
				for(Association assoc : sourceAssociations) {
					if(assoc.getId() == null) {
						assoc.setSourceEntity(entity);
						addAssociation(assoc);
					}
				}
				List<Association> targetAssociations = entity.getTargetAssociations();
				for(Association assoc : targetAssociations) {
					if(assoc.getId() == null) {
						assoc.setTargetEntity(entity);
						addAssociation(assoc);
					}
				}
				*/
				if(listener != null) {
					listener.onAfterAdd(entity);
				}
				cacheService.put("entityCache", String.valueOf(entity.getId()), entity);
			}			
		} catch(Exception e) {
			e.printStackTrace();
			throw new InvalidEntityException("entity model not found for qname:"+entity.getQName().toString(), e);			
		}
		return entity.getId();
	}
	@Override
	public Long addEntity(Long source, Long target, QName association, List<Property> properties, Entity entity) throws InvalidEntityException {
		try	{
			addEntity(entity);
			Association assoc = null;
			if(source != null) assoc = getAssociation(association, source, entity.getId());
			else if(target != null) assoc = getAssociation(association, entity.getId(), target);
			if(assoc != null) {
				if(properties != null) {
					for(Property property : properties) {
						assoc.getProperties().add(property);
					}
				}
				addAssociation(assoc);
				return assoc.getId();
			} else {
				throw new InvalidEntityException("invalid association source:"+source+" target:"+target);
			}			
		} catch(InvalidAssociationException e1) {
			e1.printStackTrace();
		} 
		return null;
	}
	public void addEntities(Collection<Entity> entities) throws InvalidEntityException {
		Map<String,Long> idMap = new HashMap<String,Long>();
		try	{
			int i = 0;
			for(Entity entity : entities) {
				addEntity(entity);
				idMap.put(entity.getUid(), entity.getId());
				i++;
				if(i % 10 == 0) System.out.println(i+" of "+entities.size()+" nodes migrated");
			}
			i = 0;
			for(Entity entity : entities) {	    		
				List<Association> sourceAssociations = entity.getSourceAssociations();
				for(Association assoc : sourceAssociations) {		    				
					try	{
						//String qname = assoc.getQName().toString();
						Long sourceid = entity.getId();
						Long targetid = assoc.getTarget();
						if(targetid == null) targetid = idMap.get(assoc.getTargetUid());
						if(sourceid != null && targetid != null) {
							assoc.setSource(sourceid);
							assoc.setTarget(targetid);
							addAssociation(assoc);
						} else {
							System.out.println("bad news on id lookup:"+sourceid+" to "+targetid);
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				List<Association> targetAssociations = entity.getTargetAssociations();
				for(Association assoc : targetAssociations) {		    				
					try	{
						//String qname = assoc.getQName().toString();
						Long sourceid = assoc.getSource();
						Long targetid = entity.getId();
						if(sourceid == null) sourceid = idMap.get(assoc.getSourceUid());
						if(sourceid != null && targetid != null) {
							assoc.setSource(sourceid);
							assoc.setTarget(targetid);
							addAssociation(assoc);
						} else {
							System.out.println("bad news on id lookup:"+sourceid+" to "+targetid);
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
				i++;
				if(i % 10 == 0) System.out.println(i+" of "+entities.size()+" nodes relationships migrated");
			}
		} catch(InvalidEntityException e) {
			throw new InvalidEntityException("", e);
		} 
	}	
	public void removeEntity(QName qname, long id) throws InvalidEntityException {
		cacheService.remove("entityCache", String.valueOf(id));
		Transaction tx = nodeService.getTransaction();
		try	{
			/** Cascade the delete to entities marked so in the data dictionary 
			 *  regardless, delete them from the cache so they are reloaded on next request. **/
			Entity entity = getEntity(qname, id);
			if(entity != null) {
				DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
				if(dictionary != null) {
					EntityPersistenceListener listener = persistenceListeners.get(entity.getQName().toString());
					if(listener != null) listener.onBeforeDelete(entity);
					
					List<QName> cascades = new ArrayList<QName>();
					Model model = dictionary.getModel(entity.getQName());
					while(model != null) {						
						for(ModelRelation relation : model.getSourceRelations()) {
							if(relation.isCascade()) {
								cascades.add(relation.getQName());
							}
						}
						model = model.getParent();
					}
					
					if(cascades.size() > 0) {
						List<Long> processedIds = new ArrayList<Long>();
						for(Association assoc : entity.getSourceAssociations()) {
							if(cascades.contains(assoc.getQName())) {
								if(assoc.getTarget() != id && !processedIds.contains(assoc.getTarget())) {
									removeEntity(assoc.getTargetName(), assoc.getTarget());
									processedIds.add(assoc.getTarget());
									cacheService.remove("entityCache", String.valueOf(assoc.getTarget()));
								}
							}
						}
					}
					for(Association assoc : entity.getTargetAssociations()) {
						removeAssociation(assoc.getId());
					}
					nodeService.removeNode(id);
					//nodeService.deindexNode(id);
					if(loggingOn) System.out.println("removing node:"+entity.getId());
					if(listener != null) listener.onAfterDelete(entity);
				}
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new InvalidEntityException("", e);
		} finally {
			tx.finish();
        }		
	}
	
	public void cascadeProperty(QName qname, Long id, QName association, QName propertyName, Serializable propertyValue) throws InvalidEntityException, InvalidPropertyException {
		Transaction tx = nodeService.getTransaction();
		try	{
			Entity entity = getEntity(qname, id);
			List<Association> associations = entity.getSourceAssociations(association);
			for(Association assoc : associations) {
				Entity targetEntity = getEntity(assoc.getTargetName(), assoc.getTarget());
				targetEntity.addProperty(propertyName, propertyValue);
				updateEntity(targetEntity);
				cascadeProperty(targetEntity.getQName(), targetEntity.getId(), association, propertyName, propertyValue);
			}
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new InvalidEntityException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public void updateEntity(Entity entity) throws InvalidEntityException {
		//log.info("received entity update id:"+entity.getId());
		Transaction tx = nodeService.getTransaction();
		try	{
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			EntityPersistenceListener listener = persistenceListeners.get(entity.getQName().toString());
			if(listener == null) listener = persistenceListeners.get("default");
			if(listener != null) {
				Entity oldValue = getEntity(entity.getQName(), entity.getId());
				listener.onBeforeUpdate(oldValue, entity);
			}
			if(entity.getId() != null) {
				if(entity.getUid() == null) entity.setUid(java.util.UUID.randomUUID().toString());
				nodeService.addUpdateNode(entity.getNode());
				List<ModelField> fields = dictionary.getModelFields(entity.getQName());
				for(ModelField f : fields) {
					Property property = entity.getProperty(f.getQName());
					if(property == null)
						nodeService.removeNodeProperty(entity.getId(), f.getQName().toString());
				}
				for(Property property : entity.getProperties()) {
					ModelField field = null;
					for(ModelField f : fields) {
						if(f.getQName().equals(property.getQName())) {
							field = f;
							break;
						}
					}						
					if(field != null) {							
						if(property.getValue() != null && (acceptEmptyValues || !property.getValue().equals(""))) {
							nodeService.addUpdateNodeProperty(entity.getId(), property.getQName().toString(), property.getValue());
						} else nodeService.removeNodeProperty(entity.getId(), property.getQName().toString());
					} else if(property.getType().equals(ModelField.TYPE_COMPUTED)) {
						nodeService.addUpdateNodeProperty(entity.getId(), property.getQName().toString(), property.getValue());
					} else if(loggingOn)
						System.out.println("Model field not found for entity : "+entity.getQName().toString()+" property : "+property.getQName().toString());
				}
				for(Association assoc : entity.getSourceAssociations()) {
					if(assoc.getId() == null) {
						assoc.setSourceEntity(entity);
						addAssociation(assoc);
					}
				}
				for(Association assoc : entity.getTargetAssociations()) {
					if(assoc.getId() == null) {
						assoc.setTargetEntity(entity);
						addAssociation(assoc);
					}
				}
				cacheService.remove("entityCache", String.valueOf(entity.getId()));
			}
			if(listener != null) {
				listener.onAfterUpdate(entity);
			}			
			tx.success();
		} catch(Exception e) {
			tx.failure();
			e.printStackTrace();
		} finally {
			tx.finish();
        }	
	}
	public Association getAssociation(long id) throws InvalidAssociationException {
		try {
			Relationship node = nodeService.getRelationship(id);
			return getAssociation(node);
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
	}
	public Association getAssociation(QName qname, long source, long target) throws InvalidAssociationException {
		try {
			Association assoc = new AssociationImpl(qname, source, target);
			assoc.setSourceName(nodeService.getNodeQName(source));
			assoc.setTargetName(nodeService.getNodeQName(target));
			return assoc;
		} catch(Exception e) {
			throw new InvalidAssociationException("", e);
		}
	}
	
	protected Association getAssociation(Relationship rel) throws InvalidAssociationException {		
		Association assoc = null;
		QName relQname =null;
		try {
			relQname = rel.getQName();
			if(!relQname.equals(OPENAPPS_ENTITY)) {
				assoc = new AssociationImpl(relQname, rel.getStartNode(), rel.getEndNode());
				assoc.setId(rel.getId());
				assoc.setSourceName(nodeService.getNodeQName(rel.getStartNode()));
				assoc.setTargetName(nodeService.getNodeQName(rel.getEndNode()));
				Map<String,Object> properties = nodeService.getRelationshipProperties(rel.getId());
				Model model = dictionaryService.getModel(relQname, assoc.getSourceName(), assoc.getTargetName());
				for(String key : properties.keySet()) {
					QName fieldQName = QName.createQualifiedName(key);
					ModelField field = model.getField(fieldQName);
					assoc.addProperty(fieldQName, field.getType(), properties.get(key));
				}
			}
		} catch(Exception e) {
			throw new InvalidAssociationException(e.getMessage());
		}
		return assoc;
	}
	
	@Override
	public Long addAssociation(Association association)	throws InvalidAssociationException {
		if((association.getSource() == null && association.getSourceEntity() == null) || (association.getTarget() == null && association.getTargetEntity() == null))
			return null;
		cacheService.remove("entityCache", String.valueOf(association.getSource()));
		cacheService.remove("entityCache", String.valueOf(association.getTarget()));
		Transaction tx = nodeService.getTransaction();
		try	{
			Entity source = association.getSourceEntity() != null ? association.getSourceEntity() : getEntity(association.getSourceName(), association.getSource());
			Entity target = association.getTargetEntity() != null ? association.getTargetEntity() : getEntity(association.getTargetName(), association.getTarget());
			association.setSourceName(source.getQName());
			association.setTargetName(target.getQName());
			/*
			boolean add = true;
			List<Association> sourceAssocs = source.getAssociations(association.getQName());
			for(Association sourceAssoc : sourceAssocs) {
				if(sourceAssoc.getTarget() == association.getTarget()) 
					add = false;
			}
			*/			
			//if(add) {
				EntityPersistenceListener startListener = persistenceListeners.get(association.getSourceName().toString());
				if(startListener == null) startListener = persistenceListeners.get("default");
				EntityPersistenceListener endListener = persistenceListeners.get(association.getTargetName().toString());
				if(endListener == null) endListener = persistenceListeners.get("default");
				startListener.onBeforeAssociationAdd(association);
				endListener.onBeforeAssociationAdd(association);
				
				Relationship relationship = nodeService.createRelationship(association.getQName(), association.getSource(), association.getTarget());
				for(Property prop : association.getProperties()) {
					nodeService.addUpdateRelationshipProperty(relationship.getId(), prop.getQName().toString(), prop.getValue());
				}
				association.setId(relationship.getId());
				source.getSourceAssociations().add(association);
				target.getTargetAssociations().add(association);
							
				startListener.onAfterAssociationAdd(association);			
				endListener.onAfterAssociationAdd(association);
				startListener.index(source);
				endListener.index(target);
			//}
			tx.success();
			return relationship.getId();
		} catch(Exception e) {
			tx.failure();
			throw new InvalidAssociationException("", e);
		} finally {
			tx.finish();
        }
	}
	@Override
	public void updateAssociation(Association association) throws InvalidAssociationException {
		cacheService.remove("entityCache", String.valueOf(association.getSource()));
		cacheService.remove("entityCache", String.valueOf(association.getTarget()));
		Transaction tx = nodeService.getTransaction();
		try	{
			//Node startNode = association.getSource() != null ? nodeService.getNode(association.getSource()) : nodeService.getNode(association.getSourceEntity().getId());
			//Node endNode = association.getTarget() != null ? nodeService.getNode(association.getTarget()) : nodeService.getNode(association.getTargetEntity().getId());
			Entity sourceEntity = getEntity(association.getSourceName(), association.getSource());
			Entity targetEntity = getEntity(association.getTargetName(), association.getTarget());
			
			association.setSourceName(sourceEntity.getQName());
			association.setTargetName(targetEntity.getQName());
						
			EntityPersistenceListener startListener = persistenceListeners.get(association.getSourceName().toString());
			if(startListener == null) startListener = persistenceListeners.get("default");
			EntityPersistenceListener endListener = persistenceListeners.get(association.getTargetName().toString());
			if(endListener == null) endListener = persistenceListeners.get("default");
			
			startListener.onBeforeAssociationUpdate(association);
			endListener.onBeforeAssociationUpdate(association);
			
			removeAssociation(association.getId());
			addAssociation(association);
			
			startListener.onAfterAssociationUpdate(association);
			endListener.onAfterAssociationUpdate(association);						
			tx.success();
		} catch(Exception e) {
			tx.failure();
			throw new InvalidAssociationException("", e);
		} finally {
			tx.finish();
        }
	}
	public void removeAssociation(long id) throws InvalidAssociationException {
		Transaction tx = nodeService.getTransaction();
		try	{
			Association association = getAssociation(id);
			Entity source = association.getSourceEntity() != null ? association.getSourceEntity() : getEntity(association.getSourceName(), association.getSource());
			Entity target = association.getTargetEntity() != null ? association.getTargetEntity() : getEntity(association.getTargetName(), association.getTarget());
			association.setSourceName(source.getQName());
			association.setTargetName(target.getQName());			
			
			EntityPersistenceListener startListener = persistenceListeners.get(association.getSourceName().toString());
			if(startListener == null) startListener = persistenceListeners.get("default");
			EntityPersistenceListener endListener = persistenceListeners.get(association.getTargetName().toString());
			if(endListener == null) endListener = persistenceListeners.get("default");
			startListener.onBeforeAssociationDelete(association);
			endListener.onBeforeAssociationDelete(association);
			
			nodeService.removeRelationship(id);
			source.getAssociations().remove(association);
			target.getAssociations().remove(association);
			
			startListener.onAfterAssociationDelete(association);
			endListener.onAfterAssociationDelete(association);
			tx.success();
			cacheService.remove("entityCache", String.valueOf(association.getSource()));
			cacheService.remove("entityCache", String.valueOf(association.getTarget()));
		} catch(Exception e) {
			tx.failure();
			throw new InvalidAssociationException("", e);
		} finally {
			tx.finish();
        }
	}
	public ValidationResult validate(Entity entity) {
		ValidationResult result = new ValidationResult();
		try {
			DataDictionary dictionary = dictionaryService.getDataDictionary(entity.getDictionary());
			Model model = dictionary.getModel(entity.getQName());
			if(model == null) result.setException(ValidationResult.MODEL_MISSING);
			else {
				for(ModelField field : model.getFields()) {
					if(field.getQName() != null && field.getQName().getNamespace() != null && field.getQName().getLocalName() != null) {
						Property property = entity.getProperty(field.getQName().getNamespace(), field.getQName().getLocalName());
						if(property == null) property = entity.getProperty("", field.getQName().getLocalName());
						if(property != null) {
							if(property.getQName().getNamespace() == null) property.getQName().setNamespace(field.getQName().getNamespace());
							if(property.toString().length() < field.getMinSize()) result.addValidationField(new ValidationField(field.getQName().getLocalName(), ValidationField.MINIMUM_VALUE_RESTRICTION));
							else if(field.getMaxSize() > 0 && property.toString().length() > field.getMaxSize()) result.addValidationField(new ValidationField(field.getQName().getLocalName(), ValidationField.MAXIMUM_VALUE_RESTRICTION));
						} else if(field.isMandatory()) result.addValidationField(new ValidationField(field.getQName().getLocalName(), ValidationField.REQUIRED_VALUE_MISSING));
					}
				}
			}
		} catch(Exception e) {
			result.setValid(false);
			result.setMessage(e.getLocalizedMessage());
			System.out.println(result.toString());
		}
		return result;
	}
	public ValidationResult validate(Association association) throws ModelValidationException {
		DataDictionary dictionary = dictionaryService.getDataDictionary(association.getDictionary());
		ValidationResult result = new ValidationResult(true);
		if(association.getSource() == 0) {
			result.setValid(false);
			result.addValidationField(new ValidationField("source", ValidationResult.MISSING_VALUE));
		}
		if(association.getTarget() == 0) {
			result.setValid(false);
			result.addValidationField(new ValidationField("target", ValidationResult.MISSING_VALUE));
		}
		if(result.isValid()) {
			try {
				Model model = dictionary.getModel(association.getSourceName());
				QName endQname = nodeService.getNodeQName(association.getTarget());
				if(model != null) {
					if(association.getQName() == null) {				
						for(ModelRelation rel : model.getRelations()) {
							if(rel.getEndName().equals(endQname)) {
								association.setQname(rel.getQName());
								result.setValid(true);
								return result;
							}
						}
					} else {
						Model m = model;
						while(m != null) {
							if(m.containsRelation(association.getQName())) {
								result.setValid(true);
								return result;
							}
							m = dictionary.getModel(m.getParentName());
						}
					}
					result.setValid(false);
					result.setMessage("Association of type "+association.getQName()+" not found.");
				}
			} catch(Exception e) {
				throw new ModelValidationException(result);
			}
		}
		return result;
	}
	
	public void registerImportProcessor(String name, ImportProcessor processor) {
		List<ImportProcessor> list = importers.get(name);
		if(list == null) {
			list = new ArrayList<ImportProcessor>();
			importers.put(name, list);
		}
		list.add(processor);
	}
	public void registerExportProcessor(String name, ExportProcessor processor) {
		exporters.put(name, processor);
	}
	public void registerEntityPersistenceListener(String name, EntityPersistenceListener component) {
		persistenceListeners.put(name, component);
	}
	@Override
	public List<ImportProcessor> getImportProcessors(String name) {
		List<ImportProcessor> processors = importers.get(name);
		if(processors == null) processors = new ArrayList<ImportProcessor>();
		if(!processors.contains(defaultImportProcessor)) processors.add(defaultImportProcessor);
		return processors;
	}
	@Override
	public ExportProcessor getExportProcessor(String name) {
		ExportProcessor processor = exporters.get(name);
		if(processor != null) return processor;
		else return exporters.get("default");
	}
	@Override
	public EntityPersistenceListener getEntityPersistenceListener(String name) {
		EntityPersistenceListener listener = persistenceListeners.get(name);
		if(listener == null ) listener = persistenceListeners.get("default");
		return listener;
	}	
	public Job cleanEntities(QName qname) throws InvalidQualifiedNameException {
		EntityCleaningJob job = new EntityCleaningJob(nodeService, qname);
		schedulingService.run(job);
		return job;
	}
	public void setImporters(Map<String, List<ImportProcessor>> importers) {
		this.importers = importers;
	}
	public void addExporter(String name, ExportProcessor exporter) {
		exporters.put(name, exporter);
	}
	public void setExporters(Map<String, ExportProcessor> exporters) {
		this.exporters = exporters;
	}
	public void addPersistenceListener(String name, EntityPersistenceListener listener) {
		persistenceListeners.put(name, listener);
	}
	public void setPersistenceListeners(Map<String, EntityPersistenceListener> persistenceListeners) {
		this.persistenceListeners = persistenceListeners;
	}	
	
	@Override
	public Object export(FormatInstructions instructions, Entity entity) throws InvalidEntityException {
		ExportProcessor defaultProcessor = getExportProcessor("default");
		ExportProcessor processor = getExportProcessor(entity.getQName().toString());		
		if(processor != null) {
			return processor.export(instructions, entity);
		} else {
			return defaultProcessor.export(instructions, entity);
		}
	}
	@Override
	public Object export(FormatInstructions instructions, Association association)	throws InvalidEntityException {
		ExportProcessor defaultProcessor = getExportProcessor("default");
		ExportProcessor processor = getExportProcessor(association.getTargetName().toString());
		if(processor != null) {
			return processor.export(instructions, association);
		} else {
			return defaultProcessor.export(instructions, association);
		}
	}
	/*
	@Override
	public Job index(QName qname) {
		EntityPersistenceListener listener = persistenceListeners.get(qname.toString());
		if(listener == null) listener = persistenceListeners.get("default");
		if(listener != null) {
			Job job = listener.index(qname);
			return job;
		}
		return null;
	}
	@Override
	public void index(Long entityId, boolean wait) {
		if(wait) {
			try {
				Node node = nodeService.getNode(entityId);
	        	if(node != null) {
	        		if(node.getQName() != null) {                			
	        			Map<String,Object> properties = nodeService.getNodeProperties(entityId);
	        			properties.put("xid", node.getXid());
	        			for(String key : properties.keySet()) {
	        				if(key.equals(SystemModel.NAME.toString())) {
	        					String value = (String)properties.get(key);
	        					if(value != null) {
	        						value = HTMLUtility.removeTags(value.replace(",", ""));
	        						properties.put(key, value);    
	        					}
	        				}
	        			}
	        			nodeService.indexNode(node.getId(), properties);
	        		}
	        	}
			} catch(Exception e) {
				e.printStackTrace();
			}
		} else indexingQueue.add(entityId);
	}
	
	@Override
	public void deindex(Long entityId) {
		if(entityId != null) {
			try {
				nodeService.deindexNode(entityId);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	*/
	public void setSchedulingService(SchedulingService schedulingService) {
		this.schedulingService = schedulingService;
	}
	public void setCacheService(CacheService cacheService) {
		this.cacheService = cacheService;
	}
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	public void setDictionaryService(DataDictionaryService dictionaryService) {
		this.dictionaryService = dictionaryService;
	}
	public void setAcceptEmptyValues(boolean acceptEmptyValues) {
		this.acceptEmptyValues = acceptEmptyValues;
	}
	public boolean isLoggingOn() {
		return loggingOn;
	}
	public void setLoggingOn(boolean loggingOn) {
		this.loggingOn = loggingOn;
	}
	@Override
	public Entity getEntity(QName qname, QName propertyQname, Object value) throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EntityResultSet getEntities(QName qname, int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EntityResultSet getEntities(QName qname, QName propertyQname, Object value, int page, int size)
			throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int count(QName qname) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Entity getEntity(long id, boolean sources, boolean targets) throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EntityResultSet getEntities(QName qname, QName propertyQname, int page, int size)
			throws InvalidEntityException {
		// TODO Auto-generated method stub
		return null;
	}

	
}
